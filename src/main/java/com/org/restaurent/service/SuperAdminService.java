package com.org.restaurent.service;

import com.org.restaurent.dto.req.SuperAdminReqDto;

import java.util.List;

public interface SuperAdminService {
    boolean insertSuperAdminService(SuperAdminReqDto superAdminReqDto);

    boolean updateSuperAdminService(SuperAdminReqDto superAdminReqDto);

    List getSuperAdminListService();

    List getSuperAdminByIdListService(Integer superAdminId);
}
