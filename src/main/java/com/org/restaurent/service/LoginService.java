package com.org.restaurent.service;

import com.org.restaurent.dto.req.EmpCredentialsReqDto;
import com.org.restaurent.dto.req.UserLoginReqDto;
import com.org.restaurent.dto.res.EmpCredentialsResDto;
import com.org.restaurent.dto.res.UserLoginResDto;


public interface LoginService {

    UserLoginResDto getUserNameLoginService(UserLoginReqDto userLoginDto);

    EmpCredentialsResDto getUserNameByMobNoService(String employeeContactNo);

    boolean updatePasswordService(EmpCredentialsReqDto empCredentialsReqDto);

//    PrivilageDto getLoginPrivilageServie(UserWiseRoleDto userWiseRoleDto);

}
