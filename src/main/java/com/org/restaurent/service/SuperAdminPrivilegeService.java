package com.org.restaurent.service;

import com.org.restaurent.dto.req.SuperAdminPrivilegeReqDto;

import java.util.List;

public interface SuperAdminPrivilegeService {
    boolean insertSuperAdminPrivilegeService(SuperAdminPrivilegeReqDto superAdminPrivilegeReqDto);

    boolean updateSuperAdminPrivilegeService(SuperAdminPrivilegeReqDto superAdminPrivilegeReqDto);

    List getSuperAdminPrivilegeListService();

    List getSuperAdminPrivilegeByIdListService(Integer superAdminPriviId);
}
