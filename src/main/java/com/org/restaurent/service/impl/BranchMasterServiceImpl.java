package com.org.restaurent.service.impl;

import com.org.restaurent.dao.BranchMasterDao;
import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.BranchMasterReqDto;
import com.org.restaurent.dto.res.BranchMasterResDto;
import com.org.restaurent.model.BranchMaster;
import com.org.restaurent.service.BranchMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BranchMasterServiceImpl implements BranchMasterService{

    @Autowired
    private BranchMasterDao branchMasterDao;

    @Autowired
    private CommonDao commonDao;


    @Override
    public boolean insertBranchMasterService(BranchMasterReqDto branchMasterReqDto) {
        BranchMaster branchMaster = new BranchMaster();

        branchMaster.setBranchName(branchMasterReqDto.getBranchName());
        branchMaster.setBranchAddr(branchMasterReqDto.getBranchAddr());
        branchMaster.setPersonContactNo(branchMasterReqDto.getPersonContactNo());
        branchMaster.setPersonName(branchMasterReqDto.getPersonName());
        branchMaster.setPersonEmailId(branchMasterReqDto.getPersonEmailId());
        branchMaster.setBranchStatus(branchMasterReqDto.getBranchStatus());
        branchMaster.setRestaurentMaster(branchMasterReqDto.getRestaurentMaster());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(branchMaster);
        Boolean flag = commonDao.createCommonDao(commonClass);


        return flag;
    }




    @Override
    public boolean updateBranchMasterService(BranchMasterReqDto branchMasterReqDto) {
        BranchMaster branchMaster = new BranchMaster();

        branchMaster.setBranchId(branchMasterReqDto.getBranchId());
        branchMaster.setBranchName(branchMasterReqDto.getBranchName());
        branchMaster.setBranchAddr(branchMasterReqDto.getBranchAddr());
        branchMaster.setPersonContactNo(branchMasterReqDto.getPersonContactNo());
        branchMaster.setPersonName(branchMasterReqDto.getPersonName());
        branchMaster.setPersonEmailId(branchMasterReqDto.getPersonEmailId());
        branchMaster.setBranchStatus(branchMasterReqDto.getBranchStatus());
        branchMaster.setRestaurentMaster(branchMasterReqDto.getRestaurentMaster());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(branchMaster);
        Boolean flag = commonDao.updateCommonDao(commonClass);


        return flag;
    }


    @Override
    public List getBranchMasterService() {

        List list = branchMasterDao.getBranchMasterListDao();

        return list;
    }

    @Override
    public BranchMasterResDto getBranchMasterByIdService(Integer branchId) {
        BranchMasterResDto branchMasterResDto = branchMasterDao.getBranchMasterByIdDao(branchId);

        return branchMasterResDto;
    }


}
