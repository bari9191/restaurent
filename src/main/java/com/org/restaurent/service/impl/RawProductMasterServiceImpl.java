package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.RawProductMasterDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.RawProductMasterReqDto;
import com.org.restaurent.model.RawProductMaster;
import com.org.restaurent.service.RawProductMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RawProductMasterServiceImpl implements RawProductMasterService {

    @Autowired
    private RawProductMasterDao rawProductMasterDao;

    @Autowired
    private CommonDao commonDao;

    @Override
    public boolean insertRawProductMasterService(RawProductMasterReqDto rawProductMasterReqDto) {

        RawProductMaster rawProductMaster=new RawProductMaster();

        rawProductMaster.setProductName(rawProductMasterReqDto.getProductName());
        rawProductMaster.setProductCost(rawProductMasterReqDto.getProductCost());
        rawProductMaster.setProductQuantity(rawProductMasterReqDto.getProductQuantity());
        rawProductMaster.setProductTotalCost(rawProductMasterReqDto.getProductTotalCost());
        rawProductMaster.setProductExpiryDate(rawProductMasterReqDto.getProductExpiryDate());
        rawProductMaster.setProductDescription(rawProductMasterReqDto.getProductDescription());
        rawProductMaster.setProductStatus(rawProductMasterReqDto.getProductStatus());
        rawProductMaster.setBranchMaster(rawProductMasterReqDto.getBranchMaster());


        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(rawProductMaster);
        Boolean flag = commonDao.createCommonDao(commonClass);

        return flag;
    }

    @Override
    public boolean updateRawProductMasterService(RawProductMasterReqDto rawProductMasterReqDto) {
        RawProductMaster rawProductMaster=new RawProductMaster();

        rawProductMaster.setProductId(rawProductMasterReqDto.getProductId());
        rawProductMaster.setProductName(rawProductMasterReqDto.getProductName());
        rawProductMaster.setProductCost(rawProductMasterReqDto.getProductCost());
        rawProductMaster.setProductQuantity(rawProductMasterReqDto.getProductQuantity());
        rawProductMaster.setProductTotalCost(rawProductMasterReqDto.getProductTotalCost());
        rawProductMaster.setProductExpiryDate(rawProductMasterReqDto.getProductExpiryDate());
        rawProductMaster.setProductDescription(rawProductMasterReqDto.getProductDescription());
        rawProductMaster.setProductStatus(rawProductMasterReqDto.getProductStatus());
        rawProductMaster.setBranchMaster(rawProductMasterReqDto.getBranchMaster());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(rawProductMaster);
        Boolean flag = commonDao.updateCommonDao(commonClass);

        return flag;
    }

    @Override
    public List getRawProductMasterListService() {
        List list = rawProductMasterDao.getRawProductMasterListDao();
        return list;
    }

    @Override
    public List getRawProductMasterByIdListService(Integer rProductId) {
        List list = rawProductMasterDao.getRawProductMasterByIdListDao(rProductId);
        return list;
    }

    @Override
    public List getRawProductMasterbranchwiseListService(Integer branchId) {

        List list = rawProductMasterDao.getRawProductMasterbranchwiseListDao(branchId);
        return list;
    }
}
