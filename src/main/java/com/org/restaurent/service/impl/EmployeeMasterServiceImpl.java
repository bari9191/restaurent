package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.EmployeeMasterDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.EmpAttendanceReqDto;
import com.org.restaurent.dto.req.EmployeeMasterReqDto;
import com.org.restaurent.dto.res.EmpAttendanceResDto;
import com.org.restaurent.model.*;
import com.org.restaurent.service.EmployeeMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeMasterServiceImpl implements EmployeeMasterService {

    @Autowired
    private EmployeeMasterDao employeeMasterDao;

    @Autowired
    private CommonDao commonDao;

    @Override
    public boolean insertEmployeeMasterService(EmployeeMasterReqDto employeeMasterReqDto) {
        EmployeeMaster employeeMaster = new EmployeeMaster();
        EmpCredentials empCredentials = new EmpCredentials();
        CommonClass commonClass = new CommonClass();
        Boolean flag = false;

        empCredentials.setUserName(employeeMasterReqDto.getEmpCredentials().getUserName());
        empCredentials.setUserPass(employeeMasterReqDto.getEmpCredentials().getUserPass());
        empCredentials.setUserLoginStatus("सक्रिय");

        commonClass.setCommonClass(empCredentials);
        flag = commonDao.createCommonDao(commonClass);

        if (flag) {

            employeeMaster.setEmployeeName(employeeMasterReqDto.getEmployeeName());
            employeeMaster.setEmployeeAddress(employeeMasterReqDto.getEmployeeAddress());
            employeeMaster.setEmployeeSalary(employeeMasterReqDto.getEmployeeSalary());
            employeeMaster.setEmployeeContactNo(employeeMasterReqDto.getEmployeeContactNo());
            employeeMaster.setEmployeeEmail(employeeMasterReqDto.getEmployeeEmail());
            employeeMaster.setDesignationMaster(employeeMasterReqDto.getDesignationMaster());
            employeeMaster.setShiftMaster(employeeMasterReqDto.getShiftMaster());
            employeeMaster.setEmpCredentials(empCredentials);

            employeeMaster.setBranchMaster(employeeMasterReqDto.getBranchMaster());

            commonClass.setCommonClass(employeeMaster);
            flag = commonDao.createCommonDao(commonClass);

        }

        if(flag){
            SalaryMaster salaryMaster = new SalaryMaster();
            salaryMaster.setFinalSalary(employeeMasterReqDto.getEmployeeSalary());
            salaryMaster.setEmployeeMaster(employeeMaster);

            commonClass.setCommonClass(salaryMaster);
            flag = commonDao.createCommonDao(commonClass);
        }


        return flag;
    }

    @Override
    public boolean updateEmployeeMasterService(EmployeeMasterReqDto employeeMasterReqDto) {
        EmployeeMaster employeeMaster = new EmployeeMaster();
        EmpCredentials empCredentials = new EmpCredentials();
        CommonClass commonClass = new CommonClass();
        Boolean flag = false;

        empCredentials.setEmpCredentialsId(employeeMasterReqDto.getEmpCredentials().getEmpCredentialsId());
        empCredentials.setUserName(employeeMasterReqDto.getEmpCredentials().getUserName());
        empCredentials.setUserPass(employeeMasterReqDto.getEmpCredentials().getUserPass());
        empCredentials.setUserLoginStatus("सक्रिय");

        commonClass.setCommonClass(empCredentials);
        flag = commonDao.updateCommonDao(commonClass);

        if (flag) {

            employeeMaster.setEmployeeId(employeeMasterReqDto.getEmployeeId());
            employeeMaster.setEmployeeName(employeeMasterReqDto.getEmployeeName());
            employeeMaster.setEmployeeAddress(employeeMasterReqDto.getEmployeeAddress());
            employeeMaster.setEmployeeSalary(employeeMasterReqDto.getEmployeeSalary());
            employeeMaster.setEmployeeContactNo(employeeMasterReqDto.getEmployeeContactNo());
            employeeMaster.setEmployeeEmail(employeeMasterReqDto.getEmployeeEmail());
            employeeMaster.setDesignationMaster(employeeMasterReqDto.getDesignationMaster());
            employeeMaster.setShiftMaster(employeeMasterReqDto.getShiftMaster());
            employeeMaster.setEmpCredentials(empCredentials);


            commonClass.setCommonClass(employeeMaster);
            flag = commonDao.updateCommonDao(commonClass);

        }


        return flag;
    }

    @Override
    public List getEmployeeMasterListService(Integer branchId) {
        List list = employeeMasterDao.getEmployeeMasterListDao(branchId);

        return list;
    }

    @Override
    public List getEmployeeMasterByIdListService(Integer employeeId) {
        List list = employeeMasterDao.getEmployeeMasterByIdListDao(employeeId);

        return list;
    }

    @Override
    public List getActiveEmployeeMasterListService(Integer branchId) {

        List list = employeeMasterDao.getActiveEmployeeMasterListDao(branchId);

        return list;
    }

    @Override
    public List<EmpAttendanceResDto> getempAttenListByDateService(EmpAttendanceReqDto empAttendanceReqDto) {

        List<EmpAttendanceResDto> finalList = new ArrayList<EmpAttendanceResDto>();

        List<EmpAttendanceResDto> list = employeeMasterDao.getempAttenListByDateDao(empAttendanceReqDto);

        for(EmpAttendanceResDto empAttendanceResDto : list){

            if(empAttendanceResDto.getAttenStatus().equals("P")){
                empAttendanceResDto.setAttenFlag(true);
            }else {
                empAttendanceResDto.setAttenFlag(false);
            }

            finalList.add(empAttendanceResDto);

        }

        if(list.size()==0){

            List<EmpAttendanceResDto> list1 = employeeMasterDao.getempDataDao(empAttendanceReqDto);

            for(EmpAttendanceResDto empAttendanceResDto : list1){

                empAttendanceResDto.setAttenStatus("A");
                empAttendanceResDto.setAttenFlag(false);

                finalList.add(empAttendanceResDto);

            }
        }

        return finalList;
    }

    @Override
    public boolean saveOrUpdateEmpAttenByDateService(EmpAttendanceReqDto empAttendanceReqDto) {

        Boolean flag=false;

        for (EmpAttendanceReqDto empAttendanceReqDto1 : empAttendanceReqDto.getEmpAttendanceReqDtoList()){

            EmpAttendance empAttendance = new EmpAttendance();
            empAttendance.setEmpAttenId(empAttendanceReqDto1.getEmpAttenId());

            if(empAttendanceReqDto1.getAttenFlag().equals(true)){
                empAttendance.setAttenStatus("P");
            }else {
                empAttendance.setAttenStatus("A");
            }

            EmployeeMaster employeeMaster = new EmployeeMaster();
            employeeMaster.setEmployeeId(empAttendanceReqDto1.getEmployeeId());

            empAttendance.setEmployeeMaster(employeeMaster);

            empAttendance.setAttenDate(empAttendanceReqDto.getAttenDate());

            if(empAttendance.getEmpAttenId()!=null){

                CommonClass commonClass = new CommonClass();
                commonClass.setCommonClass(empAttendance);
                flag = commonDao.updateCommonDao(commonClass);

            }else {

                CommonClass commonClass = new CommonClass();
                commonClass.setCommonClass(empAttendance);
                flag = commonDao.createCommonDao(commonClass);
            }

        }
        return flag;
    }

}
