package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.ExpenceMasterDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.ExpenceMasterReqDto;
import com.org.restaurent.model.ExpenceMaster;
import com.org.restaurent.service.ExpenceMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExpenceMasterServiceImpl implements ExpenceMasterService {

    @Autowired
    private ExpenceMasterDao expenceMasterDao;


    @Autowired
    private CommonDao commonDao;

    @Override
    public boolean insertExpenceMasterService(ExpenceMasterReqDto expenceMasterReqDto) {
        ExpenceMaster expenceMaster = new ExpenceMaster();

        expenceMaster.setExpenceName(expenceMasterReqDto.getExpenceName());
        expenceMaster.setExpenceCost(expenceMasterReqDto.getExpenceCost());
        expenceMaster.setExpenceDescription(expenceMasterReqDto.getExpenceDescription());
        expenceMaster.setExpenceDate(expenceMasterReqDto.getExpenceDate());
        expenceMaster.setBranchMaster(expenceMasterReqDto.getBranchMaster());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(expenceMaster);
        Boolean flag = commonDao.createCommonDao(commonClass);

        return flag;
    }

    @Override
    public boolean updateExpenceMasterService(ExpenceMasterReqDto expenceMasterReqDto) {
        ExpenceMaster expenceMaster = new ExpenceMaster();

        expenceMaster.setExpenceId(expenceMasterReqDto.getExpenceId());
        expenceMaster.setExpenceName(expenceMasterReqDto.getExpenceName());
        expenceMaster.setExpenceCost(expenceMasterReqDto.getExpenceCost());
        expenceMaster.setExpenceDescription(expenceMasterReqDto.getExpenceDescription());
        expenceMaster.setExpenceDate(expenceMasterReqDto.getExpenceDate());
        expenceMaster.setBranchMaster(expenceMasterReqDto.getBranchMaster());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(expenceMaster);
        Boolean flag = commonDao.updateCommonDao(commonClass);

        return flag;
    }

    @Override
    public List getExpenceMasterListService() {
        List list = expenceMasterDao.getExpenceMasterListDao();
        return list;
    }

    @Override
    public List getExpenceMasterByIdListService(Integer expenceId) {
        List list = expenceMasterDao.getExpenceMasterByIdListDao(expenceId);
        return list;
    }

    @Override
    public List getTotalExpencetillDateService(Integer branchId) {
        List list = expenceMasterDao.getTotalExpencetillDateDao(branchId);
        return list;
    }

    @Override
    public List getExpencebranchWiseService(Integer branchId) {
        List list = expenceMasterDao.getExpencebranchWiseDao(branchId);
        return list;
    }

}
