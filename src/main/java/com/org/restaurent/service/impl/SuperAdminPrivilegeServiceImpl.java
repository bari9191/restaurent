package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.SuperAdminPrivilegeDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.SuperAdminPrivilegeReqDto;
import com.org.restaurent.model.SuperAdminPrivilege;
import com.org.restaurent.service.SuperAdminPrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SuperAdminPrivilegeServiceImpl implements SuperAdminPrivilegeService {

    @Autowired
    private SuperAdminPrivilegeDao superAdminPrivilegeDao;

    @Autowired
    private CommonDao commonDao;

    @Override
    public boolean insertSuperAdminPrivilegeService(SuperAdminPrivilegeReqDto superAdminPrivilegeReqDto) {
        SuperAdminPrivilege superAdminPrivilege=new SuperAdminPrivilege();

        superAdminPrivilege.setSAdminPriviStatus(superAdminPrivilegeReqDto.getSAdminPriviStatus());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(superAdminPrivilege);
        Boolean flag = commonDao.createCommonDao(commonClass);

        return flag;
    }

    @Override
    public boolean updateSuperAdminPrivilegeService(SuperAdminPrivilegeReqDto superAdminPrivilegeReqDto) {
        SuperAdminPrivilege superAdminPrivilege=new SuperAdminPrivilege();

        superAdminPrivilege.setSuperAdminPriviId(superAdminPrivilegeReqDto.getSuperAdminPriviId());
        superAdminPrivilege.setSAdminPriviStatus(superAdminPrivilegeReqDto.getSAdminPriviStatus());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(superAdminPrivilege);
        Boolean flag = commonDao.updateCommonDao(commonClass);

        return flag;
    }

    @Override
    public List getSuperAdminPrivilegeListService() {
        List list = superAdminPrivilegeDao.getSuperAdminPrivilegeListDao();
        return list;
    }

    @Override
    public List getSuperAdminPrivilegeByIdListService(Integer superAdminPriviId) {
        List list = superAdminPrivilegeDao.getSuperAdminPrivilegeByIdListDao(superAdminPriviId);
        return list;
    }
}
