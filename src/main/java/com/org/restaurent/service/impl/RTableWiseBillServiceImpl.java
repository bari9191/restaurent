package com.org.restaurent.service.impl;


import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.RTableWiseBillDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.RTableWiseBillReqDto;
import com.org.restaurent.model.RtableWiseBill;
import com.org.restaurent.service.RTableWiseBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RTableWiseBillServiceImpl implements RTableWiseBillService {

    @Autowired
    private RTableWiseBillDao rTableWiseBillDao;

    @Autowired
    private CommonDao commonDao;

    @Override
    public boolean insertRTableWiseBillService(RTableWiseBillReqDto rTableWiseBillReqDto) {
        RtableWiseBill rTableWiseBill = new RtableWiseBill();

        rTableWiseBill.setFixedProductMaster(rTableWiseBillReqDto.getFixedProductMaster());
        rTableWiseBill.setRtableMaster(rTableWiseBillReqDto.getRTableMaster());
        rTableWiseBill.setRtableWiseBillPayType(rTableWiseBillReqDto.getRTableWiseBillPayType());
        rTableWiseBill.setRtableWiseBillStatus(rTableWiseBillReqDto.getRTableWiseBillStatus());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(rTableWiseBill);
        Boolean flag = commonDao.createCommonDao(commonClass);

        return flag;
    }

    @Override
    public boolean updateRTableWiseBillService(RTableWiseBillReqDto rTableWiseBillReqDto) {
        RtableWiseBill rTableWiseBill = new RtableWiseBill();

        rTableWiseBill.setRtableWiseBillId(rTableWiseBillReqDto.getRTableWiseBillId());
        rTableWiseBill.setFixedProductMaster(rTableWiseBillReqDto.getFixedProductMaster());
        rTableWiseBill.setRtableMaster(rTableWiseBillReqDto.getRTableMaster());
        rTableWiseBill.setRtableWiseBillPayType(rTableWiseBillReqDto.getRTableWiseBillPayType());
        rTableWiseBill.setRtableWiseBillStatus(rTableWiseBillReqDto.getRTableWiseBillStatus());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(rTableWiseBill);
        Boolean flag = commonDao.updateCommonDao(commonClass);

        return flag;
    }

    @Override
    public List getRTableWiseBillListService() {
        List list =rTableWiseBillDao.getRTableWiseBillListDao();
        return list;
    }

    @Override
    public List getRTableWiseBillByIdListService(Integer rTableWiseBillId) {
        List list =rTableWiseBillDao.getRTableWiseBillByIdListDao(rTableWiseBillId);
        return list;
    }
}
