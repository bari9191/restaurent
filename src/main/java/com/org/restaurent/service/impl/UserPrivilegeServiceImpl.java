package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.UserPrivilegeDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.UserPrivilegeReqDto;
import com.org.restaurent.model.UserPrivilege;
import com.org.restaurent.service.UserPrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserPrivilegeServiceImpl implements UserPrivilegeService {

    @Autowired
    private UserPrivilegeDao userPrivilegeDao;

    @Autowired
    private CommonDao commonDao;

    @Override
    public boolean insertUserPrivilegeService(UserPrivilegeReqDto userPrivilegeReqDto) {
        UserPrivilege userPrivilege=new UserPrivilege();

        userPrivilege.setUMaster(userPrivilegeReqDto.getUMaster());
        userPrivilege.setUPriviStatus(userPrivilegeReqDto.getUPriviStatus());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(userPrivilege);
        Boolean flag = commonDao.createCommonDao(commonClass);

        return flag;

    }

    @Override
    public boolean updateUserPrivilegeService(UserPrivilegeReqDto userPrivilegeReqDto) {
        UserPrivilege userPrivilege=new UserPrivilege();

        userPrivilege.setUserPriviId(userPrivilegeReqDto.getUserPriviId());
        userPrivilege.setUMaster(userPrivilegeReqDto.getUMaster());
        userPrivilege.setUPriviStatus(userPrivilegeReqDto.getUPriviStatus());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(userPrivilege);
        Boolean flag = commonDao.updateCommonDao(commonClass);

        return flag;
    }

    @Override
    public List getUserPrivilegeListService() {
        List list = userPrivilegeDao.getUserPrivilegeListDao();
        return list;
    }

    @Override
    public List getUserPrivilegeByIdListService(Integer userPriviId) {
        List list = userPrivilegeDao.getUserPrivilegeByIdListDao(userPriviId);
        return list;
    }
}
