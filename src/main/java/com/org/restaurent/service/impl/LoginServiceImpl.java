package com.org.restaurent.service.impl;

import com.org.restaurent.dao.LoginDao;
import com.org.restaurent.dto.req.EmpCredentialsReqDto;
import com.org.restaurent.dto.req.UserLoginReqDto;
import com.org.restaurent.dto.res.EmpCredentialsResDto;
import com.org.restaurent.dto.res.UserLoginResDto;
import com.org.restaurent.dto.res.UserPrivilegeResDto;
import com.org.restaurent.model.EmpCredentials;
import com.org.restaurent.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginDao loginDao;


    @Override
    public UserLoginResDto getUserNameLoginService(UserLoginReqDto userLoginReqDto) {
        UserLoginResDto userLoginResDto = new UserLoginResDto();

        userLoginResDto.setUserName(userLoginReqDto.getUserName());
        userLoginResDto.setUserPass(userLoginReqDto.getUserPass());

        userLoginResDto = loginDao.getUserNameLoginDao(userLoginReqDto);

        if(userLoginResDto!=null){

            if (userLoginResDto.getUserLoginStatus().equalsIgnoreCase("active")){

                if (userLoginResDto.getUserPass().equals(userLoginReqDto.getUserPass())) {

                    System.out.println("Verified ...");
                    UserPrivilegeResDto userPrivilegeResDto = loginDao.getLoginPrivilageDao(userLoginResDto);
                    userLoginResDto.setUserPrivilegeResDto(userPrivilegeResDto);
                    userLoginResDto.setLoginMsg("Login successful!!");
                    userLoginResDto.setLoginStatus(true);
                    return userLoginResDto;
                }else {
                    userLoginResDto = new UserLoginResDto();
                    userLoginResDto.setLoginStatus(false);
                    userLoginResDto.setLoginMsg("invalid password");
                }

            }else {
                userLoginResDto = new UserLoginResDto();
                userLoginResDto.setLoginStatus(false);
                userLoginResDto.setLoginMsg("invalid username");
            }

        }else {

            userLoginResDto = new UserLoginResDto();
            userLoginResDto.setLoginStatus(false);
            userLoginResDto.setLoginMsg("invalid ");
        }

        return userLoginResDto;
    }

    @Override
    public EmpCredentialsResDto getUserNameByMobNoService(String employeeContactNo) {
        EmpCredentialsResDto empCredentialsResDto =loginDao.getuserNameBymobileno(employeeContactNo);
        return empCredentialsResDto;
    }

    @Override
    public boolean updatePasswordService(EmpCredentialsReqDto empCredentialsReqDto) {
       boolean flag = loginDao.updatePasswordDao(empCredentialsReqDto);

        return flag;
    }

}
