package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.TableAllManagementDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.RTableMasterReqDto;
import com.org.restaurent.dto.req.RTableWiseBillReqDto;
import com.org.restaurent.dto.res.TableDashboardResDto;
import com.org.restaurent.model.FixedProductMaster;
import com.org.restaurent.model.RtableWiseBill;
import com.org.restaurent.service.TableAllManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.org.restaurent.model.RtableMaster;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TableAllManagementServiceImpl implements TableAllManagementService {

    @Autowired
    private TableAllManagementDao tableAllManagementDao;

    @Autowired
    private CommonDao commonDao;

    @Override
    public List getTableWiseItemsService(Integer rtableId) {

        List list = tableAllManagementDao.getTableWiseItemsDao(rtableId);

        return list;
    }

    @Override
    public boolean updateTableWiseOrderItemsService(Integer rtableWiseBillId) {

        Boolean flag = tableAllManagementDao.updateTableWiseOrderItemsDao(rtableWiseBillId);

        return flag;
    }

    @Override
    public boolean insertTableWiseOrderItemsService(RTableWiseBillReqDto rTableWiseBillReqDto) {

        Boolean flag=false;

        for(RTableWiseBillReqDto rTableWiseBillReqDto1 : rTableWiseBillReqDto.getRtableWiseBillReqDtoList()){

            RtableWiseBill rTableWiseBill = new RtableWiseBill();
            rTableWiseBill.setRtableWiseBillId(rTableWiseBillReqDto1.getRTableWiseBillId());
            rTableWiseBill.setProductQty(rTableWiseBillReqDto1.getProductQty());
            rTableWiseBill.setRtableWiseBillPayType(rTableWiseBillReqDto1.getRTableWiseBillPayType());
            rTableWiseBill.setRtableWiseBillStatus("सक्रिय");

            FixedProductMaster fixedProductMaster = new FixedProductMaster();
            fixedProductMaster.setProductId(rTableWiseBillReqDto1.getProductId());
            rTableWiseBill.setFixedProductMaster(fixedProductMaster);

            RtableMaster rtableMaster = new RtableMaster();
            rtableMaster.setRtableId(rTableWiseBillReqDto1.getRtableId());
            rTableWiseBill.setRtableMaster(rtableMaster);


            if(rTableWiseBillReqDto1.getRTableWiseBillId()!=null){
                rTableWiseBill.setRtableWiseBillEntryDate(rTableWiseBillReqDto1.getRTableWiseBillEntryDate());
                CommonClass commonClass = new CommonClass();
                commonClass.setCommonClass(rTableWiseBill);
                flag = commonDao.updateCommonDao(commonClass);
            }else {
                rTableWiseBill.setRtableWiseBillEntryDate(new Date());
                CommonClass commonClass = new CommonClass();
                commonClass.setCommonClass(rTableWiseBill);
                flag = commonDao.createCommonDao(commonClass);

                if(flag){

                    rTableWiseBillReqDto1.setRtableAvailability("अनुपलब्ध");
                    flag = tableAllManagementDao.updateTableStatusDao(rTableWiseBillReqDto1);

                }
            }
        }


        return flag;
    }

    @Override
    public boolean printBillService(RTableMasterReqDto rTableMasterReqDto) {

        Boolean flag=false;
        RTableWiseBillReqDto rTableWiseBillReqDto=new RTableWiseBillReqDto();

        flag = tableAllManagementDao.updateRTableWiseBillStatusDao(rTableMasterReqDto);

        if(flag){
            rTableWiseBillReqDto.setRtableAvailability("उपलब्ध");
            rTableWiseBillReqDto.setRtableId(rTableMasterReqDto.getRtableId());
            flag = tableAllManagementDao.updateTableStatusDao(rTableWiseBillReqDto);
        }


        return flag;
    }
}
