package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.ShiftMasterDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.ShiftMasterReqDto;
import com.org.restaurent.model.ShiftMaster;
import com.org.restaurent.service.ShiftMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShiftMasterServiceImpl implements ShiftMasterService {

    @Autowired
    private ShiftMasterDao shiftMasterDao;

    @Autowired
    private CommonDao commonDao;


    @Override
    public boolean insertShiftMasterService(ShiftMasterReqDto shiftMasterReqDto) {
        ShiftMaster shiftMaster = new ShiftMaster();


        shiftMaster.setShiftTimeFrom(shiftMasterReqDto.getShiftTimeFrom());
        shiftMaster.setShiftTimeTo(shiftMasterReqDto.getShiftTimeTo());
        shiftMaster.setShiftType(shiftMasterReqDto.getShiftType());
        shiftMaster.setShiftStatus(shiftMasterReqDto.getShiftStatus());
        shiftMaster.setBranchMaster(shiftMasterReqDto.getBranchMaster());
        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(shiftMaster);
        Boolean flag = commonDao.createCommonDao(commonClass);

        return flag;
    }

    @Override
    public boolean updateShiftMasterService(ShiftMasterReqDto shiftMasterReqDto) {
        ShiftMaster shiftMaster = new ShiftMaster();


        shiftMaster.setShiftId(shiftMasterReqDto.getShiftId());
        shiftMaster.setShiftTimeFrom(shiftMasterReqDto.getShiftTimeFrom());
        shiftMaster.setShiftTimeTo(shiftMasterReqDto.getShiftTimeTo());
        shiftMaster.setShiftType(shiftMasterReqDto.getShiftType());
        shiftMaster.setShiftStatus(shiftMasterReqDto.getShiftStatus());
        shiftMaster.setBranchMaster(shiftMasterReqDto.getBranchMaster());
        CommonClass commonClass = new CommonClass();

        commonClass.setCommonClass(shiftMaster);
        Boolean flag = commonDao.updateCommonDao(commonClass);

        return flag;
    }

    @Override
    public List getShiftMasterListService() {
        List list = shiftMasterDao.getShiftMasterListDao();
        return list;
    }

    @Override
    public List getShiftMasterByIdListService(Integer branchId) {
        List list = shiftMasterDao.getShiftMasterByIdListDao(branchId);
        return list;
    }
}
