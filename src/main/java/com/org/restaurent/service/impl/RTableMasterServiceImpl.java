package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.RTableMasterDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.RTableMasterReqDto;
import com.org.restaurent.model.BranchMaster;
import com.org.restaurent.model.RtableMaster;
import com.org.restaurent.service.RTableMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RTableMasterServiceImpl implements RTableMasterService {

    @Autowired
    private RTableMasterDao rTableMasterDao;


    @Autowired
    private CommonDao commonDao;


    @Override
    public boolean insertRTableMasterService(RTableMasterReqDto rTableMasterReqDto) {
        RtableMaster rTableMaster = new RtableMaster();

        rTableMaster.setRtableName(rTableMasterReqDto.getRtableName());
        rTableMaster.setRtableAvailability(rTableMasterReqDto.getRtableAvailability());
        rTableMaster.setRtableStatus(rTableMasterReqDto.getRtableStatus());
        BranchMaster branchMaster = new BranchMaster();
        branchMaster.setBranchId(1);
        rTableMaster.setBranchMaster(branchMaster);

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(rTableMaster);
        Boolean flag = commonDao.createCommonDao(commonClass);

        return flag;
    }

    @Override
    public boolean updateRTableMasterService(RTableMasterReqDto rTableMasterReqDto) {
        RtableMaster rTableMaster = new RtableMaster();

        rTableMaster.setRtableId(rTableMasterReqDto.getRtableId());
        rTableMaster.setRtableName(rTableMasterReqDto.getRtableName());
        rTableMaster.setRtableStatus(rTableMasterReqDto.getRtableStatus());
        rTableMaster.setRtableAvailability(rTableMasterReqDto.getRtableAvailability());

        BranchMaster branchMaster = new BranchMaster();
        branchMaster.setBranchId(1);
        rTableMaster.setBranchMaster(branchMaster);

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(rTableMaster);
        Boolean flag = commonDao.updateCommonDao(commonClass);

        return flag;
    }

    @Override
    public List getRTableMasterListService(Integer branchId) {
      List list = rTableMasterDao.getRTableMasterListDao(branchId);

        return list;
    }

    @Override
    public List getRTableMasterByIdListService(Integer rTableId) {
        List list = rTableMasterDao.getRTableMasterByIdListDao(rTableId);

        return list;
    }

    @Override
    public List rTableMasterActiveListService(Integer branchId) {

        List list = rTableMasterDao.rTableMasterActiveListDao(branchId);

        return list;
    }


}
