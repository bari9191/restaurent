package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.RestaurentMasterDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.RestaurentMasterReqDto;
import com.org.restaurent.dto.res.RestaurentMasterResDto;
import com.org.restaurent.model.RestaurentMaster;
import com.org.restaurent.service.RestaurentMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestaurentMasterServiceImpl implements RestaurentMasterService {


    @Autowired
   private RestaurentMasterDao restaurentMasterDao;

    @Autowired
    private CommonDao commonDao;


    @Override
    public boolean insertRestaurentMasterService(RestaurentMasterReqDto restaurentMasterReqDto) {
        RestaurentMaster restaurentMaster = new RestaurentMaster();

        restaurentMaster.setSuperAdmin(restaurentMasterReqDto.getSuperAdmin());
        restaurentMaster.setRestaurentName(restaurentMasterReqDto.getRestaurentName());
        restaurentMaster.setRestaurentAddress(restaurentMasterReqDto.getRestaurentAddress());
        restaurentMaster.setRestaurentEmail(restaurentMasterReqDto.getRestaurentEmail());
        restaurentMaster.setRestaurentContactNo1(restaurentMasterReqDto.getRestaurentContactNo1());
        restaurentMaster.setRestaurentContactNo2(restaurentMasterReqDto.getRestaurentContactNo2());
        restaurentMaster.setRestaurentOwner1(restaurentMasterReqDto.getRestaurentOwner1());
        restaurentMaster.setRestaurentOwner2(restaurentMasterReqDto.getRestaurentOwner2());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(restaurentMaster);
        Boolean flag = commonDao.createCommonDao(commonClass);


        return flag;
    }

    @Override
    public boolean updateRestaurentMasterService(RestaurentMasterReqDto restaurentMasterReqDto) {
        RestaurentMaster restaurentMaster = new RestaurentMaster();

        restaurentMaster.setRestaurentId(restaurentMasterReqDto.getRestaurentId());
        restaurentMaster.setSuperAdmin(restaurentMasterReqDto.getSuperAdmin());
        restaurentMaster.setRestaurentName(restaurentMasterReqDto.getRestaurentName());
        restaurentMaster.setRestaurentAddress(restaurentMasterReqDto.getRestaurentAddress());
        restaurentMaster.setRestaurentEmail(restaurentMasterReqDto.getRestaurentEmail());
        restaurentMaster.setRestaurentContactNo1(restaurentMasterReqDto.getRestaurentContactNo1());
        restaurentMaster.setRestaurentContactNo2(restaurentMasterReqDto.getRestaurentContactNo2());
        restaurentMaster.setRestaurentOwner1(restaurentMasterReqDto.getRestaurentOwner1());
        restaurentMaster.setRestaurentOwner2(restaurentMasterReqDto.getRestaurentOwner2());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(restaurentMaster);
        Boolean flag = commonDao.updateCommonDao(commonClass);
        return flag;

    }

    @Override
    public List getRestaurentMasterListService() {
        List list = restaurentMasterDao.getRestaurentMasterListDao();
        return list;
    }

    @Override
    public RestaurentMasterResDto getRestaurentMasterByIdListService(Integer restaurentId) {
        RestaurentMasterResDto restaurentMasterResDto = restaurentMasterDao.getRestaurentMasterByIdListDao(restaurentId);
        return restaurentMasterResDto;
    }
    }

