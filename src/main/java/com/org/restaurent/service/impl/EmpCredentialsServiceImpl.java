package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.EmpCredentialsDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.EmpCredentialsReqDto;
import com.org.restaurent.model.EmpCredentials;
import com.org.restaurent.service.EmpCredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmpCredentialsServiceImpl implements EmpCredentialsService {

    @Autowired
    private EmpCredentialsDao empCredentialsDao;

    @Autowired
    private CommonDao commonDao;


    @Override
    public boolean insertEmpCredentialsService(EmpCredentialsReqDto empCredentialsReqDto) {
        EmpCredentials empCredentials=new EmpCredentials();

        empCredentials.setUserName(empCredentialsReqDto.getUserName());
        empCredentials.setUserPass(empCredentialsReqDto.getUserPass());
        empCredentials.setUserLoginStatus(empCredentialsReqDto.getUserLoginStatus());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(empCredentials);
        Boolean flag = commonDao.createCommonDao(commonClass);

        return flag;
    }

    @Override
    public boolean updateEmpCredentialsService(EmpCredentialsReqDto empCredentialsReqDto) {
        EmpCredentials empCredentials=new EmpCredentials();

        empCredentials.setEmpCredentialsId(empCredentialsReqDto.getEmpCredentialsId());
        empCredentials.setUserName(empCredentialsReqDto.getUserName());
        empCredentials.setUserPass(empCredentialsReqDto.getUserPass());
        empCredentials.setUserLoginStatus(empCredentialsReqDto.getUserLoginStatus());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(empCredentials);
        Boolean flag = commonDao.updateCommonDao(commonClass);

        return flag;
    }

    @Override
    public List getEmpCredentialsListService() {
        List list = empCredentialsDao.getEmpCredentialsListDao();
        return list;
    }

    @Override
    public List getEmpCredentialsByIdListService(Integer empCredentialsId) {
        List list = empCredentialsDao.getEmpCredentialsByIdListDao(empCredentialsId);
        return list;
    }


}

