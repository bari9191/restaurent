package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.SalaryMasterDao;
import com.org.restaurent.dao.SuperAdminDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.SalaryMasterReqDto;
import com.org.restaurent.model.SalaryMaster;
import com.org.restaurent.service.SalaryMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalaryMasterServiceImpl implements SalaryMasterService {

    @Autowired
    private SalaryMasterDao salaryMasterDao;

    @Autowired
    private CommonDao commonDao;

    @Override
    public boolean insertSalaryMasterService(SalaryMasterReqDto salaryMasterReqDto) {

        SalaryMaster salaryMaster=new SalaryMaster();

        salaryMaster.setFinalSalary(salaryMasterReqDto.getFinalSalary());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(salaryMaster);
        Boolean flag = commonDao.createCommonDao(commonClass);

        return flag;
    }

    @Override
    public boolean updateSalaryMasterService(SalaryMasterReqDto salaryMasterReqDto) {
        SalaryMaster salaryMaster=new SalaryMaster();

        salaryMaster.setSalaryId(salaryMasterReqDto.getSalaryId());
        salaryMaster.setFinalSalary(salaryMasterReqDto.getFinalSalary());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(salaryMaster);
        Boolean flag = commonDao.updateCommonDao(commonClass);

        return flag;
    }

    @Override
    public List getSalaryMasterListService() {
        List list = salaryMasterDao.getSalaryMasterListDao();
        return list;
    }

    @Override
    public List getSalaryMasterByIdListService(Integer salaryId) {
        List list = salaryMasterDao.getSalaryMasterByIdListDao(salaryId);
        return list;

    }
}
