package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.FixedProductMasterDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.FixedProductMasterReqDto;
import com.org.restaurent.model.FixedProductMaster;
import com.org.restaurent.service.FixedProductMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class FixedProductMasterServiceImpl implements FixedProductMasterService {

    @Autowired
    private FixedProductMasterDao fixedProductMasterDao;

    @Autowired
    private CommonDao commonDao;

    @Override
    public boolean insertFixedProductMasterService(FixedProductMasterReqDto fixedProductMasterReqDto) {
        FixedProductMaster fixedProductMaster = new FixedProductMaster();

        fixedProductMaster.setProductName(fixedProductMasterReqDto.getProductName());
        fixedProductMaster.setProductCost(fixedProductMasterReqDto.getProductCost());
        fixedProductMaster.setProductStatus(fixedProductMasterReqDto.getProductStatus());
        fixedProductMaster.setBranchMaster(fixedProductMasterReqDto.getBranchMaster());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(fixedProductMaster);
        Boolean flag = commonDao.createCommonDao(commonClass);


        return flag;
    }

    @Override
    public boolean updateFixedProductMasterService(FixedProductMasterReqDto fixedProductMasterReqDto) {
        FixedProductMaster fixedProductMaster = new FixedProductMaster();

        fixedProductMaster.setProductId(fixedProductMasterReqDto.getProductId());
        fixedProductMaster.setProductName(fixedProductMasterReqDto.getProductName());
        fixedProductMaster.setProductCost(fixedProductMasterReqDto.getProductCost());
        fixedProductMaster.setProductStatus(fixedProductMasterReqDto.getProductStatus());
        fixedProductMaster.setBranchMaster(fixedProductMasterReqDto.getBranchMaster());


        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(fixedProductMaster);
        Boolean flag = commonDao.updateCommonDao(commonClass);


        return flag;
    }

    @Override
    public List getFixedProductMasterListService(int branchId) {
        List list = fixedProductMasterDao.getFixedProductMasterListDao(branchId);
        return list;
    }

    @Override
    public List getFixedProductMasterByIdListService(Integer productId) {
        List list = fixedProductMasterDao.getFixedProductMasterByIdListDao(productId);
        return list;
    }

    @Override
    public List getfixedProductMasterActiveListService(int branchId) {
        List list = fixedProductMasterDao.getfixedProductMasterActiveListDao(branchId);
        return list;
    }

}
