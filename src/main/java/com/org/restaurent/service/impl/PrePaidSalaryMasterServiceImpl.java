package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.PrePaidSalaryMasterDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.PrePaidSalaryMasterReqDto;
import com.org.restaurent.model.PrePaidSalaryMaster;
import com.org.restaurent.service.PrePaidSalaryMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrePaidSalaryMasterServiceImpl implements PrePaidSalaryMasterService {

    @Autowired
    private PrePaidSalaryMasterDao prePaidSalaryMasterDao;

    @Autowired
    private CommonDao commonDao;


    @Override
    public boolean insertPrePaidSalaryMasterService(PrePaidSalaryMasterReqDto prePaidSalaryMasterReqDto) {

        PrePaidSalaryMaster prePaidSalaryMaster=new PrePaidSalaryMaster();

        prePaidSalaryMaster.setPrePaidSalaryCost(prePaidSalaryMasterReqDto.getPrePaidSalaryCost());
        prePaidSalaryMaster.setPrePaidSalaryDate(prePaidSalaryMasterReqDto.getPrePaidSalaryDate());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(prePaidSalaryMaster);
        Boolean flag = commonDao.createCommonDao(commonClass);

        return flag;
    }

    @Override
    public boolean updatePrePaidSalaryMasterService(PrePaidSalaryMasterReqDto prePaidSalaryMasterReqDto) {
        PrePaidSalaryMaster prePaidSalaryMaster=new PrePaidSalaryMaster();

        prePaidSalaryMaster.setPrePaidSalaryId(prePaidSalaryMasterReqDto.getPrePaidSalaryId());
        prePaidSalaryMaster.setPrePaidSalaryCost(prePaidSalaryMasterReqDto.getPrePaidSalaryCost());
        prePaidSalaryMaster.setPrePaidSalaryDate(prePaidSalaryMasterReqDto.getPrePaidSalaryDate());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(prePaidSalaryMaster);
        Boolean flag = commonDao.updateCommonDao(commonClass);

        return flag;
    }

    @Override
    public List getPrePaidSalaryMasterListService() {
       List list = prePaidSalaryMasterDao.getPrePaidSalaryMasterListDao();
        return list;
    }

    @Override
    public List getPrePaidSalaryMasterByIdListService(Integer prePaidSalaryId) {
        List list = prePaidSalaryMasterDao.getPrePaidSalaryMasterByIdListDao(prePaidSalaryId);
        return list;
    }
}
