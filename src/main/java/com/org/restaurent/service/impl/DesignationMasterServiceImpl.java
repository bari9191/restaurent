package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.DesignationMasterDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.DesignationMasterReqDto;
import com.org.restaurent.model.DesignationMaster;
import com.org.restaurent.service.DesignationMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DesignationMasterServiceImpl implements DesignationMasterService {

    @Autowired
    private DesignationMasterDao designationMasterDao;

    @Autowired
    private CommonDao commonDao;

    @Override
    public boolean insertDesignationMasterService(DesignationMasterReqDto designationMasterReqDto) {
        DesignationMaster designationMaster=new DesignationMaster();

        designationMaster.setDesignationName(designationMasterReqDto.getDesignationName());
        designationMaster.setDesignationStatus(designationMasterReqDto.getDesignationStatus());
        designationMaster.setRestaurentMaster(designationMasterReqDto.getRestaurentMaster());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(designationMaster);
        Boolean flag = commonDao.createCommonDao(commonClass);


        return flag;
    }

    @Override
    public boolean updateDesignationMasterService(DesignationMasterReqDto designationMasterReqDto) {
        DesignationMaster designationMaster=new DesignationMaster();

        designationMaster.setDesignationId(designationMasterReqDto.getDesignationId());
        designationMaster.setDesignationName(designationMasterReqDto.getDesignationName());
        designationMaster.setDesignationStatus(designationMasterReqDto.getDesignationStatus());
        designationMaster.setRestaurentMaster(designationMasterReqDto.getRestaurentMaster());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(designationMaster);
        Boolean flag = commonDao.updateCommonDao(commonClass);


        return flag;
    }

    @Override
    public List getDesignationMasterListService() {
        List list = designationMasterDao.getDesignationMasterListDao();
        return list;
    }

    @Override
    public List getDesignationMasterByIdListService(Integer restaurentId) {
        List list = designationMasterDao.getDesignationMasterByIdListDao(restaurentId);
        return list;
    }


}
