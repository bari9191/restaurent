package com.org.restaurent.service.impl;

import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dao.SuperAdminDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import com.org.restaurent.dto.req.SuperAdminReqDto;
import com.org.restaurent.model.SuperAdmin;
import com.org.restaurent.service.SuperAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SuperAdminServiceImpl implements SuperAdminService {

    @Autowired
    private SuperAdminDao superAdminDao;

    @Autowired
    private CommonDao commonDao;

    @Override
    public boolean insertSuperAdminService(SuperAdminReqDto superAdminReqDto) {
        SuperAdmin superAdmin=new SuperAdmin();

        superAdmin.setSAdminName(superAdminReqDto.getSAdminName());
        superAdmin.setSAdminContactNo(superAdminReqDto.getSAdminContactNo());
        superAdmin.setSAdminPassword(superAdminReqDto.getSAdminPassword());
        superAdmin.setSAdminUserName(superAdminReqDto.getSAdminUserName());
        superAdmin.setSAdminStatus("Active");
        superAdmin.setSuperAdminPrivilege(superAdminReqDto.getSuperAdminPrivilege());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(superAdmin);
        Boolean flag = commonDao.createCommonDao(commonClass);

        return flag;
    }

    @Override
    public boolean updateSuperAdminService(SuperAdminReqDto superAdminReqDto) {
        SuperAdmin superAdmin=new SuperAdmin();

        superAdmin.setSuperAdminId(superAdminReqDto.getSuperAdminId());
        superAdmin.setSAdminName(superAdminReqDto.getSAdminName());
        superAdmin.setSAdminContactNo(superAdminReqDto.getSAdminContactNo());
        superAdmin.setSAdminPassword(superAdminReqDto.getSAdminPassword());
        superAdmin.setSAdminUserName(superAdminReqDto.getSAdminUserName());
        superAdmin.setSAdminStatus(superAdminReqDto.getSAdminStatus());
        superAdmin.setSuperAdminPrivilege(superAdminReqDto.getSuperAdminPrivilege());

        CommonClass commonClass = new CommonClass();
        commonClass.setCommonClass(superAdmin);
        Boolean flag = commonDao.updateCommonDao(commonClass);

        return flag;
    }

    @Override
    public List getSuperAdminListService() {
        List list = superAdminDao.getSuperAdminListDao();
        return list;
    }

    @Override
    public List getSuperAdminByIdListService(Integer superAdminId) {
        List list = superAdminDao.getSuperAdminByIdListDao(superAdminId);
        return list;
    }
}
