package com.org.restaurent.service;

import com.org.restaurent.dto.req.FixedProductMasterReqDto;

import java.util.List;

public interface FixedProductMasterService {
    boolean insertFixedProductMasterService(FixedProductMasterReqDto fixedProductMasterReqDto);

    boolean updateFixedProductMasterService(FixedProductMasterReqDto fixedProductMasterReqDto);

    List getFixedProductMasterListService(int branchId);

    List getFixedProductMasterByIdListService(Integer fproductId);

    List getfixedProductMasterActiveListService(int branchId);
}
