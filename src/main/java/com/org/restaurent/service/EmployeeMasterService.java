package com.org.restaurent.service;

import com.org.restaurent.dto.req.EmpAttendanceReqDto;
import com.org.restaurent.dto.req.EmployeeMasterReqDto;

import java.util.List;

public interface EmployeeMasterService {
    boolean insertEmployeeMasterService(EmployeeMasterReqDto employeeMasterReqDto);

    boolean updateEmployeeMasterService(EmployeeMasterReqDto employeeMasterReqDto);

    List getEmployeeMasterListService(Integer branchId);

    List getEmployeeMasterByIdListService(Integer employeeId);

    List getActiveEmployeeMasterListService(Integer branchId);

    List getempAttenListByDateService(EmpAttendanceReqDto empAttendanceReqDto);

    boolean saveOrUpdateEmpAttenByDateService(EmpAttendanceReqDto empAttendanceReqDto);
}
