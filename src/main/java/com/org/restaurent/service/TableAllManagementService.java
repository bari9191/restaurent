package com.org.restaurent.service;

import com.org.restaurent.dto.req.RTableMasterReqDto;
import com.org.restaurent.dto.req.RTableWiseBillReqDto;
import com.org.restaurent.dto.res.TableDashboardResDto;

import java.util.List;

public interface TableAllManagementService {

    List getTableWiseItemsService(Integer rtableId);

    boolean updateTableWiseOrderItemsService(Integer rtableWiseBillId);

    boolean insertTableWiseOrderItemsService(RTableWiseBillReqDto rTableWiseBillReqDto);

    boolean printBillService(RTableMasterReqDto rTableMasterReqDto);
}
