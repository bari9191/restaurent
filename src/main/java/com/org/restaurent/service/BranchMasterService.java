package com.org.restaurent.service;

import com.org.restaurent.dto.req.BranchMasterReqDto;
import com.org.restaurent.dto.res.BranchMasterResDto;

import java.util.List;

public interface BranchMasterService {
    List getBranchMasterService();

    boolean updateBranchMasterService(BranchMasterReqDto branchMasterReqDto);

    boolean insertBranchMasterService(BranchMasterReqDto branchMasterReqDto);

    BranchMasterResDto getBranchMasterByIdService(Integer branchId);
}
