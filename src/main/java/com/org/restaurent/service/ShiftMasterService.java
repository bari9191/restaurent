package com.org.restaurent.service;

import com.org.restaurent.dto.req.ShiftMasterReqDto;

import java.util.List;

public interface ShiftMasterService {
    boolean insertShiftMasterService(ShiftMasterReqDto shiftMasterReqDto);

    boolean updateShiftMasterService(ShiftMasterReqDto shiftMasterReqDto);

    List getShiftMasterListService();

    List getShiftMasterByIdListService(Integer branchId);
}
