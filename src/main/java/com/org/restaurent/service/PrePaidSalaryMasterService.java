package com.org.restaurent.service;


import com.org.restaurent.dto.req.PrePaidSalaryMasterReqDto;

import java.util.List;

public interface PrePaidSalaryMasterService {
    boolean insertPrePaidSalaryMasterService(PrePaidSalaryMasterReqDto prePaidSalaryMasterReqDto);

    boolean updatePrePaidSalaryMasterService(PrePaidSalaryMasterReqDto prePaidSalaryMasterReqDto);

    List getPrePaidSalaryMasterListService();

    List getPrePaidSalaryMasterByIdListService(Integer prePaidSalaryId);
}
