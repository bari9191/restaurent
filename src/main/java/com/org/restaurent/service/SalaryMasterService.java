package com.org.restaurent.service;

import com.org.restaurent.dto.req.SalaryMasterReqDto;

import java.util.List;

public interface SalaryMasterService {
    boolean insertSalaryMasterService(SalaryMasterReqDto salaryMasterReqDto);

    boolean updateSalaryMasterService(SalaryMasterReqDto salaryMasterReqDto);

    List getSalaryMasterListService();

    List getSalaryMasterByIdListService(Integer salaryId);
}
