package com.org.restaurent.service;

import com.org.restaurent.dto.req.RTableWiseBillReqDto;

import java.util.List;

public interface RTableWiseBillService {
    boolean insertRTableWiseBillService(RTableWiseBillReqDto rTableWiseBillReqDto);

    boolean updateRTableWiseBillService(RTableWiseBillReqDto rTableWiseBillReqDto);

    List getRTableWiseBillListService();

    List getRTableWiseBillByIdListService(Integer rTableWiseBillId);
}
