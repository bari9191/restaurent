package com.org.restaurent.service;

import com.org.restaurent.dto.req.ExpenceMasterReqDto;

import java.util.List;

public interface ExpenceMasterService {
    boolean insertExpenceMasterService(ExpenceMasterReqDto expenceMasterReqDto);

    boolean updateExpenceMasterService(ExpenceMasterReqDto expenceMasterReqDto);

    List getExpenceMasterListService();

    List getExpenceMasterByIdListService(Integer expenceId);


    List getTotalExpencetillDateService(Integer branchId);

    List getExpencebranchWiseService(Integer branchId);
}
