package com.org.restaurent.service;


import com.org.restaurent.dto.req.RTableMasterReqDto;

import java.util.List;

public interface RTableMasterService {


    boolean insertRTableMasterService(RTableMasterReqDto rTableMasterResDto);

    boolean updateRTableMasterService(RTableMasterReqDto rTableMasterReqDto);

    List getRTableMasterListService(Integer branchId);

    List getRTableMasterByIdListService(Integer rTableId);

    List rTableMasterActiveListService(Integer branchId);
}
