package com.org.restaurent.service;

import com.org.restaurent.dto.req.EmpCredentialsReqDto;

import java.util.List;

public interface EmpCredentialsService {
  
 
    boolean insertEmpCredentialsService(EmpCredentialsReqDto empCredentialsReqDto);

    boolean updateEmpCredentialsService(EmpCredentialsReqDto empCredentialsReqDto);

    List getEmpCredentialsListService();

    List getEmpCredentialsByIdListService(Integer empCredentialsId);
}
