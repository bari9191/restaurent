package com.org.restaurent.service;


import com.org.restaurent.dto.req.UserPrivilegeReqDto;

import java.util.List;

public interface UserPrivilegeService {
    boolean insertUserPrivilegeService(UserPrivilegeReqDto userPrivilegeReqDto);

    boolean updateUserPrivilegeService(UserPrivilegeReqDto userPrivilegeReqDto);

    List getUserPrivilegeListService();

    List getUserPrivilegeByIdListService(Integer userPriviId);
}
