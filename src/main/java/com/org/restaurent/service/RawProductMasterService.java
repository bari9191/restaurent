package com.org.restaurent.service;


import com.org.restaurent.dto.req.RawProductMasterReqDto;

import java.util.List;

public interface RawProductMasterService {
    boolean insertRawProductMasterService(RawProductMasterReqDto rawProductMasterReqDto);

    boolean updateRawProductMasterService(RawProductMasterReqDto rawProductMasterReqDto);

    List getRawProductMasterListService();

    List getRawProductMasterByIdListService(Integer rProductId);

    List getRawProductMasterbranchwiseListService(Integer branchId);
}
