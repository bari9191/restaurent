package com.org.restaurent.service;

import com.org.restaurent.dto.req.RestaurentMasterReqDto;
import com.org.restaurent.dto.res.RestaurentMasterResDto;

import java.util.List;

public interface RestaurentMasterService {

    boolean insertRestaurentMasterService(RestaurentMasterReqDto restaurentMasterReqDto);

    boolean updateRestaurentMasterService(RestaurentMasterReqDto restaurentMasterReqDto);

    List getRestaurentMasterListService();

    RestaurentMasterResDto getRestaurentMasterByIdListService(Integer restaurentId);
}
