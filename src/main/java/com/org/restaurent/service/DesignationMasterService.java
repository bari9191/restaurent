package com.org.restaurent.service;

import com.org.restaurent.dto.req.DesignationMasterReqDto;

import java.util.List;

public interface DesignationMasterService {
    boolean insertDesignationMasterService(DesignationMasterReqDto designationMasterReqDto);

    boolean updateDesignationMasterService(DesignationMasterReqDto designationMasterReqDto);

    List getDesignationMasterListService();

    List getDesignationMasterByIdListService(Integer restaurentId);
}
