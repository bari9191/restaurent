package com.org.restaurent.controller;


import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.ExpenceMasterReqDto;
import com.org.restaurent.dto.res.ExpenceMasterResDto;
import com.org.restaurent.service.ExpenceMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class ExpenceMasterController {

    @Autowired
    private ExpenceMasterService expenceMasterService;


    @RequestMapping(value="ExpenceMaster" ,method= RequestMethod.POST)
    public ResponseVO<ExpenceMasterReqDto> InsertExpenceMasterCtrl(@RequestBody  ExpenceMasterReqDto expenceMasterReqDto){

        ResponseVO<ExpenceMasterReqDto> responseVO = new ResponseVO<ExpenceMasterReqDto>();

        boolean flag=expenceMasterService.insertExpenceMasterService(expenceMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }

    @RequestMapping(value="ExpenceMaster" ,method= RequestMethod.PUT)
    public ResponseVO<ExpenceMasterResDto> updateExpenceMasterCtrl(@RequestBody ExpenceMasterReqDto expenceMasterReqDto){

        ResponseVO<ExpenceMasterResDto> responseVO = new ResponseVO<ExpenceMasterResDto>();

        boolean flag=expenceMasterService.updateExpenceMasterService(expenceMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;
    }


    @RequestMapping(value = "ExpenceMasterList",method = RequestMethod.GET)
    public ResponseVO<List> getExpenceMasterListCtrl(){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=expenceMasterService.getExpenceMasterListService();

        responseVO.setResult(list);

        return responseVO;
    }


    @RequestMapping(value = "ExpenceMasterByIdList/{expenceId}",method = RequestMethod.GET)
    public ResponseVO<List> getExpenceMasterByIdListCtrl(@PathVariable Integer expenceId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=expenceMasterService.getExpenceMasterByIdListService(expenceId);

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "getTotalExpencetillDate/{branchId}",method = RequestMethod.GET)
    public ResponseVO<List> getTotalExpencetillDate(@PathVariable Integer branchId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=expenceMasterService.getTotalExpencetillDateService(branchId);

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "getExpencebranchWise/{branchId}",method = RequestMethod.GET)
    public ResponseVO<List> getExpencebranchWise(@PathVariable Integer branchId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=expenceMasterService.getExpencebranchWiseService(branchId);

        responseVO.setResult(list);

        return responseVO;
    }


}
