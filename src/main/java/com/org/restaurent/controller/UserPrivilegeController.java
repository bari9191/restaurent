package com.org.restaurent.controller;

import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.SuperAdminReqDto;
import com.org.restaurent.dto.req.UserPrivilegeReqDto;
import com.org.restaurent.dto.res.SuperAdminResDto;
import com.org.restaurent.dto.res.UserPrivilegeResDto;
import com.org.restaurent.service.UserPrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class UserPrivilegeController {

    @Autowired
    private UserPrivilegeService userPrivilegeService;

    @RequestMapping(value="UserPrivilege" ,method= RequestMethod.POST)
    public ResponseVO<UserPrivilegeResDto> insertUserPrivilegeCtrl(@RequestBody UserPrivilegeReqDto userPrivilegeReqDto){

        ResponseVO<UserPrivilegeResDto> responseVO = new ResponseVO<UserPrivilegeResDto>();

        boolean flag=userPrivilegeService.insertUserPrivilegeService(userPrivilegeReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }

    @RequestMapping(value="UserPrivilege" ,method= RequestMethod.PUT)
    public ResponseVO<UserPrivilegeResDto> updateUserPrivilegeCtrl(@RequestBody UserPrivilegeReqDto userPrivilegeReqDto){

        ResponseVO<UserPrivilegeResDto> responseVO = new ResponseVO<UserPrivilegeResDto>();

        boolean flag=userPrivilegeService.updateUserPrivilegeService(userPrivilegeReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;
    }


    @RequestMapping(value = "UserPrivilegeList",method = RequestMethod.GET)
    public ResponseVO<List> getUserPrivilegeListCtrl(){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=userPrivilegeService.getUserPrivilegeListService();

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "UserPrivilegeByIdList/{userPriviId}",method = RequestMethod.GET)
    public ResponseVO<List> getUserPrivilegeByIdListCtrl(@PathVariable Integer userPriviId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=userPrivilegeService.getUserPrivilegeByIdListService(userPriviId);

        responseVO.setResult(list);

        return responseVO;
    }
}
