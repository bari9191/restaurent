package com.org.restaurent.controller;

import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.EmpCredentialsReqDto;
import com.org.restaurent.dto.req.SuperAdminReqDto;
import com.org.restaurent.dto.res.EmpCredentialsResDto;
import com.org.restaurent.dto.res.SuperAdminResDto;
import com.org.restaurent.service.EmpCredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class EmpCredentialsController {

    @Autowired
    private EmpCredentialsService empCredentialsService;

    @RequestMapping(value="EmpCredentials" ,method= RequestMethod.POST)
    public ResponseVO<EmpCredentialsResDto> insertEmpCredentialsCtrl(@RequestBody EmpCredentialsReqDto empCredentialsReqDto){

        ResponseVO<EmpCredentialsResDto> responseVO = new ResponseVO<EmpCredentialsResDto>();

        boolean flag=empCredentialsService.insertEmpCredentialsService(empCredentialsReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }

    @RequestMapping(value="EmpCredentials" ,method= RequestMethod.PUT)
    public ResponseVO<EmpCredentialsResDto> updateEmpCredentialsCtrl(@RequestBody EmpCredentialsReqDto empCredentialsReqDto){

        ResponseVO<EmpCredentialsResDto> responseVO = new ResponseVO<EmpCredentialsResDto>();

        boolean flag=empCredentialsService.updateEmpCredentialsService(empCredentialsReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;
    }
    @RequestMapping(value = "EmpCredentialsList",method = RequestMethod.GET)
    public ResponseVO<List> getEmpCredentialsListCtrl(){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=empCredentialsService.getEmpCredentialsListService();

        responseVO.setResult(list);

        return responseVO;
    }


    @RequestMapping(value = "EmpCredentialsByIdList/{empCredentialsId}",method = RequestMethod.GET)
    public ResponseVO<List> getEmpCredentialsByIdListCtrl(@PathVariable Integer empCredentialsId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=empCredentialsService.getEmpCredentialsByIdListService(empCredentialsId);

        responseVO.setResult(list);

        return responseVO;
    }
}
