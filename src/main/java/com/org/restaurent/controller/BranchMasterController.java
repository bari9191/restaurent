package com.org.restaurent.controller;


import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.BranchMasterReqDto;
import com.org.restaurent.dto.req.RestaurentMasterReqDto;
import com.org.restaurent.dto.res.BranchMasterResDto;
import com.org.restaurent.dto.res.RestaurentMasterResDto;
import com.org.restaurent.service.BranchMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class BranchMasterController {

    @Autowired
    private BranchMasterService branchMasterService;




    @RequestMapping(value="BranchMaster" ,method= RequestMethod.POST)
    public ResponseVO<BranchMasterReqDto> BranchMasterCtrl(@RequestBody BranchMasterReqDto branchMasterReqDto){

        System.out.println("data"+branchMasterReqDto.getBranchName());
        ResponseVO<BranchMasterReqDto> responseVO = new ResponseVO<BranchMasterReqDto>();

        boolean flag=branchMasterService.insertBranchMasterService(branchMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }

    @RequestMapping(value="branchMaster" ,method= RequestMethod.PUT)
    public ResponseVO<BranchMasterResDto> updateBranchMasterCtrl(@RequestBody BranchMasterReqDto branchMasterReqDto){

        ResponseVO<BranchMasterResDto> responseVO = new ResponseVO<BranchMasterResDto>();

        boolean flag=branchMasterService.updateBranchMasterService(branchMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;
    }


    @RequestMapping(value = "BranchMasterList",method = RequestMethod.GET)
    public ResponseVO<List> getBranchMasterListCtrl(){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=branchMasterService.getBranchMasterService();

        responseVO.setResult(list);

        return responseVO;
    }




    @RequestMapping(value = "branchMasterById/{branchId}",method = RequestMethod.GET)
    public ResponseVO<BranchMasterResDto> getBranchMasterByIdCtrl(@PathVariable Integer branchId){
        ResponseVO<BranchMasterResDto> responseVO=new ResponseVO<BranchMasterResDto>();

        BranchMasterResDto branchMasterResDto=branchMasterService.getBranchMasterByIdService(branchId);

        responseVO.setResult(branchMasterResDto);

        return responseVO;
    }



}
