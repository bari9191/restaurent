package com.org.restaurent.controller;

import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.SalaryMasterReqDto;
import com.org.restaurent.dto.req.SuperAdminReqDto;
import com.org.restaurent.dto.res.SalaryMasterResDto;
import com.org.restaurent.dto.res.SuperAdminResDto;
import com.org.restaurent.service.SalaryMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class SalaryMasterController {

    @Autowired
    private SalaryMasterService salaryMasterService;

    @RequestMapping(value="SalaryMaster" ,method= RequestMethod.POST)
    public ResponseVO<SalaryMasterResDto> insertSuperAdminCtrl(@RequestBody SalaryMasterReqDto salaryMasterReqDto){

        ResponseVO<SalaryMasterResDto> responseVO = new ResponseVO<SalaryMasterResDto>();

        boolean flag=salaryMasterService.insertSalaryMasterService(salaryMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }
    @RequestMapping(value="SalaryMaster" ,method= RequestMethod.PUT)
    public ResponseVO<SalaryMasterResDto> updateSalaryMasterCtrl(@RequestBody SalaryMasterReqDto salaryMasterReqDto){

        ResponseVO<SalaryMasterResDto> responseVO = new ResponseVO<SalaryMasterResDto>();

        boolean flag=salaryMasterService.updateSalaryMasterService(salaryMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;
    }


    @RequestMapping(value = "salaryMasterList",method = RequestMethod.GET)
    public ResponseVO<List> getSalaryMasterListCtrl(){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=salaryMasterService.getSalaryMasterListService();

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "salaryMasterByIdList/{salaryId} ",method = RequestMethod.GET)
    public ResponseVO<List> getSalaryMasterByIdListCtrl(@PathVariable Integer salaryId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=salaryMasterService.getSalaryMasterByIdListService(salaryId);

        responseVO.setResult(list);

        return responseVO;
    }
}
