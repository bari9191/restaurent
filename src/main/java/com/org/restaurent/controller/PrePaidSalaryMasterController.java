package com.org.restaurent.controller;

import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.PrePaidSalaryMasterReqDto;
import com.org.restaurent.dto.req.SuperAdminReqDto;
import com.org.restaurent.dto.res.PrePaidSalaryMasterResDto;
import com.org.restaurent.dto.res.SuperAdminResDto;
import com.org.restaurent.service.PrePaidSalaryMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class PrePaidSalaryMasterController {

    @Autowired
    private PrePaidSalaryMasterService prePaidSalaryMasterService;

    @RequestMapping(value="PrePaidSalaryMaster" ,method= RequestMethod.POST)
    public ResponseVO<PrePaidSalaryMasterResDto> insertPrePaidSalaryMasterCtrl(@RequestBody PrePaidSalaryMasterReqDto prePaidSalaryMasterReqDto){

        ResponseVO<PrePaidSalaryMasterResDto> responseVO = new ResponseVO<PrePaidSalaryMasterResDto>();

        boolean flag=prePaidSalaryMasterService.insertPrePaidSalaryMasterService(prePaidSalaryMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }

    @RequestMapping(value="PrePaidSalaryMaster" ,method= RequestMethod.PUT)
    public ResponseVO<PrePaidSalaryMasterResDto> updatePrePaidSalaryMasterCtrl(@RequestBody PrePaidSalaryMasterReqDto prePaidSalaryMasterReqDto){

        ResponseVO<PrePaidSalaryMasterResDto> responseVO = new ResponseVO<PrePaidSalaryMasterResDto>();

        boolean flag=prePaidSalaryMasterService.updatePrePaidSalaryMasterService(prePaidSalaryMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;
    }


    @RequestMapping(value = "PrePaidSalaryMasterList",method = RequestMethod.GET)
    public ResponseVO<List> getPrePaidSalaryMasterListCtrl(){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=prePaidSalaryMasterService.getPrePaidSalaryMasterListService();

        responseVO.setResult(list);

        return responseVO;
    }


    @RequestMapping(value = "PrePaidSalaryMasterByIdList/{prePaidSalaryId}",method = RequestMethod.GET)
    public ResponseVO<List> getPrePaidSalaryMasterByIdListCtrl(@PathVariable Integer prePaidSalaryId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=prePaidSalaryMasterService.getPrePaidSalaryMasterByIdListService(prePaidSalaryId);

        responseVO.setResult(list);

        return responseVO;
    }
}
