package com.org.restaurent.controller;

import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.EmpAttendanceReqDto;
import com.org.restaurent.dto.req.EmployeeMasterReqDto;
import com.org.restaurent.dto.res.EmpAttendanceResDto;
import com.org.restaurent.dto.res.EmployeeMasterResDto;
import com.org.restaurent.service.EmployeeMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class EmployeeMasterController {

    @Autowired
    private EmployeeMasterService employeeMasterService;


    @RequestMapping(value = "EmployeeMaster", method = RequestMethod.POST)
    public ResponseVO<EmployeeMasterResDto> insertEmployeeMasterCtrl(@RequestBody EmployeeMasterReqDto employeeMasterReqDto) {

        ResponseVO<EmployeeMasterResDto> responseVO = new ResponseVO<EmployeeMasterResDto>();

        boolean flag = employeeMasterService.insertEmployeeMasterService(employeeMasterReqDto);
        if (flag) {
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        } else {
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;


    }

    @RequestMapping(value = "EmployeeMaster", method = RequestMethod.PUT)
    public ResponseVO<EmployeeMasterResDto> updateEmployeeMasterCtrl(@RequestBody EmployeeMasterReqDto employeeMasterReqDto) {

        ResponseVO<EmployeeMasterResDto> responseVO = new ResponseVO<EmployeeMasterResDto>();

        boolean flag = employeeMasterService.updateEmployeeMasterService(employeeMasterReqDto);
        if (flag) {
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        } else {
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;


    }

    @RequestMapping(value = "employeeMasterList/{branchId}",method = RequestMethod.GET)
    public ResponseVO<List> getEmployeeMasterListCtrl(@PathVariable Integer branchId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=employeeMasterService.getEmployeeMasterListService(branchId);

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "activeEmployeeMasterList/{branchId}",method = RequestMethod.GET)
    public ResponseVO<List> getActiveEmployeeMasterListCtrl(@PathVariable Integer branchId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=employeeMasterService.getActiveEmployeeMasterListService(branchId);

        responseVO.setResult(list);

        return responseVO;
    }


    @RequestMapping(value = "employeeMasterByIdList/{employeeId}",method = RequestMethod.GET)
    public ResponseVO<List> getEmployeeMasterByIdListCtrl(@PathVariable Integer employeeId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=employeeMasterService.getEmployeeMasterByIdListService(employeeId);

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "empAttenListByDate",method = RequestMethod.POST)
    public ResponseVO<List> getempAttenListByDateCtrl(@RequestBody EmpAttendanceReqDto empAttendanceReqDto){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List<EmpAttendanceResDto> list=employeeMasterService.getempAttenListByDateService(empAttendanceReqDto);

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "saveOrUpdateEmpAttenByDate", method = RequestMethod.POST)
    public ResponseVO<EmployeeMasterResDto> saveOrUpdateEmpAttenByDateCtrl(@RequestBody EmpAttendanceReqDto empAttendanceReqDto) {

        ResponseVO<EmployeeMasterResDto> responseVO = new ResponseVO<EmployeeMasterResDto>();

        boolean flag = employeeMasterService.saveOrUpdateEmpAttenByDateService(empAttendanceReqDto);
        if (flag) {
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        } else {
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;


    }
}