package com.org.restaurent.controller;


import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.RTableWiseBillReqDto;
import com.org.restaurent.dto.res.RTableWiseBillResDto;
import com.org.restaurent.service.RTableWiseBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class RTableWiseBillController {

    @Autowired
    private RTableWiseBillService rTableWiseBillService;



    @RequestMapping(value="RTableWiseBill" ,method= RequestMethod.POST)
    public ResponseVO<RTableWiseBillReqDto> RTableWiseBillCtrl(@RequestBody RTableWiseBillReqDto rTableWiseBillReqDto){

        ResponseVO<RTableWiseBillReqDto> responseVO = new ResponseVO<RTableWiseBillReqDto>();

        boolean flag=rTableWiseBillService.insertRTableWiseBillService(rTableWiseBillReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }

    @RequestMapping(value="RTableWiseBill" ,method= RequestMethod.PUT)
    public ResponseVO<RTableWiseBillResDto> updateRTableWiseBillCtrl(@RequestBody RTableWiseBillReqDto rTableWiseBillReqDto){

        ResponseVO<RTableWiseBillResDto> responseVO = new ResponseVO<RTableWiseBillResDto>();

        boolean flag=rTableWiseBillService.updateRTableWiseBillService(rTableWiseBillReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;
    }


    @RequestMapping(value = "RTableWiseBillList",method = RequestMethod.GET)
    public ResponseVO<List> getRTableWiseBillListCtrl(){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=rTableWiseBillService.getRTableWiseBillListService();

        responseVO.setResult(list);

        return responseVO;
    }


    @RequestMapping(value = "RTableWiseBillByIdList/{rTableWiseBillId}",method = RequestMethod.GET)
    public ResponseVO<List> getRTableWiseBillByIdListCtrl(@PathVariable Integer rTableWiseBillId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=rTableWiseBillService.getRTableWiseBillByIdListService(rTableWiseBillId);

        responseVO.setResult(list);

        return responseVO;
    }



}
