package com.org.restaurent.controller;


import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.RTableMasterReqDto;
import com.org.restaurent.dto.res.RTableMasterResDto;
import com.org.restaurent.service.RTableMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class RTableMasterController {

    @Autowired
    private RTableMasterService rTableMasterService;




    @RequestMapping(value="rTableMaster" ,method= RequestMethod.POST)
    public ResponseVO<RTableMasterReqDto> RTableMasterCtrl(@RequestBody RTableMasterReqDto rtableMasterResDto){

        ResponseVO<RTableMasterReqDto> responseVO = new ResponseVO<RTableMasterReqDto>();

        boolean flag=rTableMasterService.insertRTableMasterService(rtableMasterResDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }

    @RequestMapping(value="rTableMaster" ,method= RequestMethod.PUT)
    public ResponseVO<RTableMasterResDto> updateRTableMasterCtrl(@RequestBody RTableMasterReqDto rTableMasterReqDto){

        ResponseVO<RTableMasterResDto> responseVO = new ResponseVO<RTableMasterResDto>();

        boolean flag=rTableMasterService.updateRTableMasterService(rTableMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;
    }


    @RequestMapping(value = "rTableMasterList/{branchId}",method = RequestMethod.GET)
    public ResponseVO<List> getRTableMasterListCtrl(@PathVariable Integer branchId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=rTableMasterService.getRTableMasterListService(branchId);

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "rTableMasterActiveList/{branchId}",method = RequestMethod.GET)
    public ResponseVO<List> rTableMasterActiveListCtrl(@PathVariable Integer branchId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=rTableMasterService.rTableMasterActiveListService(branchId);

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "RTableMasterByIdList/{rTableId}",method = RequestMethod.GET)
    public ResponseVO<List> getRTableMasterByIdListCtrl(@PathVariable Integer rTableId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=rTableMasterService.getRTableMasterByIdListService(rTableId);

        responseVO.setResult(list);

        return responseVO;
    }


}
