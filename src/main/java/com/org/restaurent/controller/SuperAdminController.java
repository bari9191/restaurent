package com.org.restaurent.controller;

import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.SuperAdminReqDto;
import com.org.restaurent.dto.res.SuperAdminResDto;
import com.org.restaurent.service.SuperAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class SuperAdminController {

    @Autowired
    private SuperAdminService superAdminService;


    @RequestMapping(value="SuperAdmin" ,headers = "Accept=application/json",method= RequestMethod.POST)

    public ResponseVO<SuperAdminResDto> insertSuperAdminCtrl(@RequestBody SuperAdminReqDto superAdminReqDto){
        System.out.println("data inside controller super admin"+superAdminReqDto.getSAdminName());
        ResponseVO<SuperAdminResDto> responseVO = new ResponseVO<SuperAdminResDto>();




        boolean flag=superAdminService.insertSuperAdminService(superAdminReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }

       @RequestMapping(value="SuperAdmin" ,method= RequestMethod.PUT)
    public ResponseVO<SuperAdminResDto> updateSuperAdminCtrl(@RequestBody SuperAdminReqDto superAdminReqDto){

        ResponseVO<SuperAdminResDto> responseVO = new ResponseVO<SuperAdminResDto>();

        boolean flag=superAdminService.updateSuperAdminService(superAdminReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;
    }


    @RequestMapping(value = "SuperAdminList",method = RequestMethod.GET)
    public ResponseVO<List> getSuperAdminListCtrl(){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=superAdminService.getSuperAdminListService();

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "SuperAdminByIdList/{superAdminId}",method = RequestMethod.GET)
    public ResponseVO<List> getSuperAdminByIdListCtrl(@PathVariable Integer superAdminId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=superAdminService.getSuperAdminByIdListService(superAdminId);

        responseVO.setResult(list);

        return responseVO;
    }

}
