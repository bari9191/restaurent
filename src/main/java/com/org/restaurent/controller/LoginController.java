package com.org.restaurent.controller;

import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.EmpCredentialsReqDto;
import com.org.restaurent.dto.req.UserLoginReqDto;
import com.org.restaurent.dto.res.EmpCredentialsResDto;
import com.org.restaurent.dto.res.EmployeeMasterResDto;
import com.org.restaurent.dto.res.UserLoginResDto;
import com.org.restaurent.model.EmpCredentials;
import com.org.restaurent.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @RequestMapping(value="userLogin",method= RequestMethod.POST)
    public ResponseVO<UserLoginResDto> getUserNameForLoginCtrl(@RequestBody UserLoginReqDto userLoginReqDto)
    {
        System.out.println("Inside Rest Controller");

        ResponseVO<UserLoginResDto> responseVO = new ResponseVO<UserLoginResDto>();

        UserLoginResDto userLoginResDto = loginService.getUserNameLoginService(userLoginReqDto);

        if(userLoginResDto!=null)
        {
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            System.out.println(responseVO.getStatusCode());
            responseVO.setMessage(userLoginResDto.getLoginMsg());
            responseVO.setResult(userLoginResDto);
        }
        else
        {
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            System.out.println(responseVO.getStatusCode());
            responseVO.setMessage(userLoginResDto.getLoginMsg());
            System.out.println("msg:"+responseVO.getMessage());
            responseVO.setResult(userLoginResDto);

        }
        return responseVO;
    }


    @RequestMapping(value = "getUserNameByMobNo/{employeeContactNo}",method = RequestMethod.GET)
    public ResponseVO<EmpCredentialsResDto> getUserNameByMobNoCtrl(@PathVariable String employeeContactNo){
        ResponseVO<EmpCredentialsResDto> responseVO=new ResponseVO<EmpCredentialsResDto>();
        EmpCredentialsResDto empCredentialsResDto =loginService.getUserNameByMobNoService(employeeContactNo);
        responseVO.setResult(empCredentialsResDto);
        if(empCredentialsResDto!=null)
        { responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            System.out.println(responseVO.getStatusCode());
            responseVO.setMessage("succesfull");
            responseVO.setResult(empCredentialsResDto);
        } else
        { responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            System.out.println(responseVO.getStatusCode());
            responseVO.setMessage("Failed");
            System.out.println("msg:"+responseVO.getMessage());
            responseVO.setResult(empCredentialsResDto);
        }
        return responseVO;
    }

    @RequestMapping(value = "/changePassword1",method = RequestMethod.PUT)
    public ResponseVO<EmpCredentialsResDto> updatePasswordCtrl(@PathVariable EmpCredentialsReqDto empCredentialsReqDto){

        ResponseVO<EmpCredentialsResDto> responseVO = new ResponseVO<EmpCredentialsResDto>();
        boolean flag=loginService.updatePasswordService(empCredentialsReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Password Updated Sucessfully");
        }
        else {
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Failed to Update Password");
        }
        return responseVO;
    }


//    @RequestMapping(value = "loginPrivilage" , method = RequestMethod.POST)
//    public ResponseVO<PrivilageDto> getLoginPrivilageCtrl (@RequestBody UserWiseRoleDto userWiseRoleDto) {
//
//        ResponseVO responseVO = new ResponseVO();
//        PrivilageDto privilageDto = null;
//        privilageDto = loginService.getLoginPrivilageServie(userWiseRoleDto);
//
//        if (privilageDto != null) {
//            responseVO.setStatusCode(String.valueOf(org.apache.http.HttpStatus.SC_OK));
//            responseVO.setMessage("Get Succesfully");
//            responseVO.setResult(privilageDto);
//        } else {
//            responseVO.setStatusCode(String.valueOf(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR));
//            responseVO.setMessage("Get Failed");
//            responseVO.setResult(privilageDto);
//        }
//        return responseVO;
//    }

}
