package com.org.restaurent.controller;


import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.RestaurentMasterReqDto;
import com.org.restaurent.dto.res.RestaurentMasterResDto;
import com.org.restaurent.service.RestaurentMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class RestaurentMasterController {

    @Autowired
    private RestaurentMasterService restaurentMasterService;



    @RequestMapping(value="RestaurentMaster" ,method= RequestMethod.POST)
    public ResponseVO<RestaurentMasterReqDto> RestaurentMasterCtrl(@RequestBody RestaurentMasterReqDto restaurentMasterReqDto){

        ResponseVO<RestaurentMasterReqDto> responseVO = new ResponseVO<RestaurentMasterReqDto>();

        boolean flag=restaurentMasterService.insertRestaurentMasterService(restaurentMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }

    @RequestMapping(value="restaurentMaster" ,method= RequestMethod.PUT)
    public ResponseVO<RestaurentMasterResDto> updateRestaurentMasterCtrl(@RequestBody RestaurentMasterReqDto restaurentMasterReqDto){

        ResponseVO<RestaurentMasterResDto> responseVO = new ResponseVO<RestaurentMasterResDto>();

        boolean flag=restaurentMasterService.updateRestaurentMasterService(restaurentMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;
    }


    @RequestMapping(value = "RestaurentMasterList",method = RequestMethod.GET)
    public ResponseVO<List> getRestaurentMasterListCtrl(){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=restaurentMasterService.getRestaurentMasterListService();

        responseVO.setResult(list);

        return responseVO;
    }


    @RequestMapping(value = "restaurentMasterById/{restaurentId}",method = RequestMethod.GET)
    public ResponseVO<RestaurentMasterResDto> getRestaurentMasterByIdListCtrl(@PathVariable Integer restaurentId){
        ResponseVO<RestaurentMasterResDto> responseVO=new ResponseVO<RestaurentMasterResDto>();

        RestaurentMasterResDto restaurentMasterResDto=restaurentMasterService.getRestaurentMasterByIdListService(restaurentId);

        responseVO.setResult(restaurentMasterResDto);

        return responseVO;
    }
}
