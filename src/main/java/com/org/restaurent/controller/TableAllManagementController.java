package com.org.restaurent.controller;

import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.BranchMasterReqDto;
import com.org.restaurent.dto.req.DesignationMasterReqDto;
import com.org.restaurent.dto.req.RTableMasterReqDto;
import com.org.restaurent.dto.req.RTableWiseBillReqDto;
import com.org.restaurent.dto.res.TableDashboardResDto;
import com.org.restaurent.service.TableAllManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class TableAllManagementController {

    @Autowired
    private TableAllManagementService tableAllManagementService;

    @RequestMapping(value = "tableWiseOrderItems/{rtableId}",method = RequestMethod.GET)
    public ResponseVO<List> getTableWiseOrderItemsListCtrl(@PathVariable Integer rtableId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List tableDashboardResDtoList=tableAllManagementService.getTableWiseItemsService(rtableId);

        responseVO.setResult(tableDashboardResDtoList);

        return responseVO;
    }

    @RequestMapping(value="tableWiseOrderItems/{rtableWiseBillId}" ,method= RequestMethod.PUT)
    public ResponseVO<BranchMasterReqDto> updateTableWiseOrderItemsCtrl(@PathVariable Integer rtableWiseBillId){

        ResponseVO<BranchMasterReqDto> responseVO = new ResponseVO<BranchMasterReqDto>();

        boolean flag=tableAllManagementService.updateTableWiseOrderItemsService(rtableWiseBillId);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Deleted successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Delete Failed!!");
        }
        return responseVO;
    }

    @RequestMapping(value="tableWiseOrderItems" ,method= RequestMethod.POST)
    public ResponseVO<DesignationMasterReqDto> insertTableWiseOrderItemsCtrl(@RequestBody RTableWiseBillReqDto rTableWiseBillReqDto){

        ResponseVO<DesignationMasterReqDto> responseVO = new ResponseVO<DesignationMasterReqDto>();

        boolean flag=tableAllManagementService.insertTableWiseOrderItemsService(rTableWiseBillReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }

    @RequestMapping(value="printBill" ,method= RequestMethod.PUT)
    public ResponseVO<DesignationMasterReqDto> printBillCtrl(@RequestBody RTableMasterReqDto rTableMasterReqDto){

        ResponseVO<DesignationMasterReqDto> responseVO = new ResponseVO<DesignationMasterReqDto>();

        boolean flag=tableAllManagementService.printBillService(rTableMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;
    }

}
