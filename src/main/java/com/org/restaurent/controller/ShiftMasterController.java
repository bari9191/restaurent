package com.org.restaurent.controller;


import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.RestaurentMasterReqDto;
import com.org.restaurent.dto.req.ShiftMasterReqDto;
import com.org.restaurent.dto.res.RestaurentMasterResDto;
import com.org.restaurent.dto.res.ShiftMasterResDto;
import com.org.restaurent.service.ShiftMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class ShiftMasterController {


    @Autowired
    private ShiftMasterService shiftMasterService;


    @RequestMapping(value="ShiftMaster" ,method= RequestMethod.POST)
    public ResponseVO<ShiftMasterReqDto> InsertShiftMasterCtrl(@RequestBody ShiftMasterReqDto shiftMasterReqDto){

        ResponseVO<ShiftMasterReqDto> responseVO = new ResponseVO<ShiftMasterReqDto>();

        boolean flag=shiftMasterService.insertShiftMasterService(shiftMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }

    @RequestMapping(value="ShiftMaster" ,method= RequestMethod.PUT)
    public ResponseVO<ShiftMasterResDto> updateShiftMasterCtrl(@RequestBody ShiftMasterReqDto shiftMasterReqDto){

        ResponseVO<ShiftMasterResDto> responseVO = new ResponseVO<ShiftMasterResDto>();

        boolean flag=shiftMasterService.updateShiftMasterService(shiftMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;
    }


    @RequestMapping(value = "ShiftMasterList",method = RequestMethod.GET)
    public ResponseVO<List> getShiftMasterListCtrl(){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=shiftMasterService.getShiftMasterListService();

        responseVO.setResult(list);

        return responseVO;
    }


    @RequestMapping(value = "ShiftMasterByIdList/{branchId}",method = RequestMethod.GET)
    public ResponseVO<List> getShiftMasterByIdListCtrl(@PathVariable Integer branchId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=shiftMasterService.getShiftMasterByIdListService(branchId);

        responseVO.setResult(list);

        return responseVO;
    }
}
