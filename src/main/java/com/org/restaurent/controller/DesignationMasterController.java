package com.org.restaurent.controller;



import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.DesignationMasterReqDto;
import com.org.restaurent.dto.req.SuperAdminReqDto;
import com.org.restaurent.dto.res.DesignationMasterResDto;
import com.org.restaurent.dto.res.SuperAdminResDto;
import com.org.restaurent.service.DesignationMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class DesignationMasterController {

    @Autowired
    private DesignationMasterService designationMasterService;


    @RequestMapping(value="designationMaster" ,method= RequestMethod.POST)
    public ResponseVO<DesignationMasterReqDto> insertDesignationMasterCtrl(@RequestBody DesignationMasterReqDto designationMasterReqDto){

        ResponseVO<DesignationMasterReqDto> responseVO = new ResponseVO<DesignationMasterReqDto>();

        System.out.println("restaurent ID"+designationMasterReqDto.getRestaurentMaster().getRestaurentId());
        boolean flag=designationMasterService.insertDesignationMasterService(designationMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }

    @RequestMapping(value="DesignationMaster" ,method= RequestMethod.PUT)
    public ResponseVO<DesignationMasterResDto> updateDesignationMasterCtrl(@RequestBody DesignationMasterReqDto designationMasterReqDto){

        ResponseVO<DesignationMasterResDto> responseVO = new ResponseVO<DesignationMasterResDto>();

        boolean flag=designationMasterService.updateDesignationMasterService(designationMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;
    }


    @RequestMapping(value = "DesignationMasterList",method = RequestMethod.GET)
    public ResponseVO<List> getDesignationMasterListCtrl(){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=designationMasterService.getDesignationMasterListService();

        responseVO.setResult(list);

        return responseVO;
    }


    @RequestMapping(value = "DesignationMasterByIdList/{restaurentId}",method = RequestMethod.GET)
    public ResponseVO<List> getDesignationMasterByIdListCtrl(@PathVariable Integer restaurentId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=designationMasterService.getDesignationMasterByIdListService(restaurentId);

        responseVO.setResult(list);

        return responseVO;
    }

}
