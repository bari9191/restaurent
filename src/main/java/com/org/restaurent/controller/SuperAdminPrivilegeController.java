package com.org.restaurent.controller;

import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.SuperAdminPrivilegeReqDto;
import com.org.restaurent.dto.req.SuperAdminReqDto;
import com.org.restaurent.dto.res.SuperAdminPrivilegeResDto;
import com.org.restaurent.dto.res.SuperAdminResDto;
import com.org.restaurent.service.SuperAdminPrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class SuperAdminPrivilegeController {

    @Autowired
    private SuperAdminPrivilegeService superAdminPrivilegeService;


    @RequestMapping(value="SuperAdminPrivilege" ,method= RequestMethod.POST)
    public ResponseVO<SuperAdminPrivilegeResDto> insertSuperAdminPrivilegeCtrl(@RequestBody SuperAdminPrivilegeReqDto superAdminPrivilegeReqDto){

        ResponseVO<SuperAdminPrivilegeResDto> responseVO = new ResponseVO<SuperAdminPrivilegeResDto>();

        boolean flag=superAdminPrivilegeService.insertSuperAdminPrivilegeService(superAdminPrivilegeReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }

    @RequestMapping(value="SuperAdminPrivilege" ,method= RequestMethod.PUT)
    public ResponseVO<SuperAdminPrivilegeResDto> updateSuperAdminPrivilegeCtrl(@RequestBody SuperAdminPrivilegeReqDto superAdminPrivilegeReqDto){

        ResponseVO<SuperAdminPrivilegeResDto> responseVO = new ResponseVO<SuperAdminPrivilegeResDto>();

        boolean flag=superAdminPrivilegeService.updateSuperAdminPrivilegeService(superAdminPrivilegeReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;
    }


    @RequestMapping(value = "SuperAdminPrivilegeList",method = RequestMethod.GET)
    public ResponseVO<List> getSuperAdminListCtrl(){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=superAdminPrivilegeService.getSuperAdminPrivilegeListService();

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "SuperAdminPrivilegeByIdList/{superAdminPriviId}",method = RequestMethod.GET)
    public ResponseVO<List> getSuperAdminByIdListCtrl(@PathVariable Integer superAdminPriviId){
            ResponseVO<List>responseVO = new ResponseVO<List>();

            List list=superAdminPrivilegeService.getSuperAdminPrivilegeByIdListService(superAdminPriviId);

            responseVO.setResult(list);

            return responseVO;
            }
}
