package com.org.restaurent.controller;

import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.RawProductMasterReqDto;
import com.org.restaurent.dto.req.SuperAdminReqDto;
import com.org.restaurent.dto.res.RawProductMasterResDto;
import com.org.restaurent.dto.res.SuperAdminResDto;
import com.org.restaurent.service.RawProductMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin (origins = "*")
public class RawProductMasterController {

    @Autowired
    private RawProductMasterService rawProductMasterService;


    @RequestMapping(value="RawProductMaster" ,method= RequestMethod.POST)
    public ResponseVO<RawProductMasterResDto> insertRawProductMasterCtrl(@RequestBody RawProductMasterReqDto rawProductMasterReqDto){

        System.out.println(rawProductMasterReqDto.getProductName());
        ResponseVO<RawProductMasterResDto> responseVO = new ResponseVO<RawProductMasterResDto>();

        boolean flag=rawProductMasterService.insertRawProductMasterService(rawProductMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;
    }

    @RequestMapping(value="RawProductMaster" ,method= RequestMethod.PUT)
    public ResponseVO<RawProductMasterResDto> updateRawProductMasterCtrl(@RequestBody RawProductMasterReqDto rawProductMasterReqDto){

        ResponseVO<RawProductMasterResDto> responseVO = new ResponseVO<RawProductMasterResDto>();

        boolean flag=rawProductMasterService.updateRawProductMasterService(rawProductMasterReqDto);

        if(flag){
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Update successfully");
        }
        else{
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Update Failed!!");
        }
        return responseVO;

    }


    @RequestMapping(value = "RawProductMasterList",method = RequestMethod.GET)
    public ResponseVO<List> getRawProductMasterCtrl(){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=rawProductMasterService.getRawProductMasterListService();

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "RawProductMasterByIdList/{rProductId}",method = RequestMethod.GET)
    public ResponseVO<List> getRawProductMasterByIdListCtrl(@PathVariable Integer rProductId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=rawProductMasterService.getRawProductMasterByIdListService(rProductId);

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "RawProductMasterbranchwise/{branchId}",method = RequestMethod.GET)
    public ResponseVO<List> RawProductMasterbranchwise(@PathVariable Integer branchId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=rawProductMasterService.getRawProductMasterbranchwiseListService(branchId);

        responseVO.setResult(list);

        return responseVO;
    }

}
