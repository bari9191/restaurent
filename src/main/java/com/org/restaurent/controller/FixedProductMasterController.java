package com.org.restaurent.controller;


import com.org.restaurent.dto.dtoconfiguration.ResponseVO;
import com.org.restaurent.dto.req.FixedProductMasterReqDto;
import com.org.restaurent.dto.res.FixedProductMasterResDto;
import com.org.restaurent.service.FixedProductMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class FixedProductMasterController {

    @Autowired
    private FixedProductMasterService fixedProductMasterService;

    @RequestMapping(value = "fixedProductMaster", method = RequestMethod.POST)
    public ResponseVO<FixedProductMasterResDto> FixedProductMasterCtrl(@RequestBody FixedProductMasterReqDto fixedProductMasterReqDto) {

        ResponseVO<FixedProductMasterResDto> responseVO = new ResponseVO<FixedProductMasterResDto>();

        boolean flag = fixedProductMasterService.insertFixedProductMasterService(fixedProductMasterReqDto);
        if (flag) {
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        } else {
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;


    }

    @RequestMapping(value = "fixedProductMaster", method = RequestMethod.PUT)
    public ResponseVO<FixedProductMasterResDto> UpdateFixedProductMasterCtrl(@RequestBody FixedProductMasterReqDto fixedProductMasterReqDto) {

        ResponseVO<FixedProductMasterResDto> responseVO = new ResponseVO<FixedProductMasterResDto>();

        boolean flag = fixedProductMasterService.updateFixedProductMasterService(fixedProductMasterReqDto);
        if (flag) {
            responseVO.setStatusCode(String.valueOf(HttpStatus.OK));
            responseVO.setMessage("Insert successfully");
        } else {
            responseVO.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            responseVO.setMessage("Insert Failed!!");
        }
        return responseVO;

    }



    @RequestMapping(value = "fixedProductMasterList/{branchId}",method = RequestMethod.GET)
    public ResponseVO<List> getListCtrl(@PathVariable int branchId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=fixedProductMasterService.getFixedProductMasterListService(branchId);

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "fixedProductMasterActiveList/{branchId}",method = RequestMethod.GET)
    public ResponseVO<List> getfixedProductMasterActiveListCtrl(@PathVariable int branchId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=fixedProductMasterService.getfixedProductMasterActiveListService(branchId);

        responseVO.setResult(list);

        return responseVO;
    }

    @RequestMapping(value = "fixedProductMaster/{productId}",method = RequestMethod.GET)
    public ResponseVO<List> getListbyIdCtrl(@PathVariable int productId){
        ResponseVO<List> responseVO=new ResponseVO<List>();

        List list=fixedProductMasterService.getFixedProductMasterByIdListService(productId);

        responseVO.setResult(list);

        return responseVO;
    }


}
