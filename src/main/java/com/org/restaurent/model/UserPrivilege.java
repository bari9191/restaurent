package com.org.restaurent.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "user_privilege")
public class UserPrivilege {

    @Id
    @Column(name = "user_privi_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer userPriviId;

    @Column(name = "u_master")
    private Boolean uMaster;

    @Column(name = "u_privi_status")
        private String uPriviStatus;

    @JoinColumn(name = "designation_id",referencedColumnName = "designation_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private DesignationMaster designationMaster;
}
