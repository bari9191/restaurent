package com.org.restaurent.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "salary_master")
public class SalaryMaster {

    @Id
    @Column(name = "salary_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer salaryId;

    @Column(name = "final_salary")
    private String finalSalary;

    @JoinColumn(name = "employee_id",referencedColumnName = "employee_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private EmployeeMaster employeeMaster;

}
