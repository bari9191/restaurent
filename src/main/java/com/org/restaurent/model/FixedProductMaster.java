package com.org.restaurent.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "fixed_product_master")
public class FixedProductMaster {

    @Id
    @Column(name = "f_product_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer productId;

    @Column(name = "f_product_name")
    private String productName;

    @Column(name = "f_product_cost")
    private Double productCost;

    @Column(name = "f_product_status")
    private String productStatus;

    @JoinColumn(name = "branch_id",referencedColumnName = "branch_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private BranchMaster branchMaster;

    @JsonIgnore
    @OneToMany(mappedBy = "fixedProductMaster",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<RtableWiseBill> rTableWiseBill;



}
