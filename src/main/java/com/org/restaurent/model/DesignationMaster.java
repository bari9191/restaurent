package com.org.restaurent.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "designation_master")
public class DesignationMaster {

    @Id
    @Column(name = "designation_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer designationId;

    @Column(name = "designation_name")
    private String designationName;

    @Column(name = "designation_status")
    private String designationStatus;

    @JoinColumn(name = "restaurent_id",referencedColumnName = "restaurent_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private RestaurentMaster restaurentMaster;

    @JsonIgnore
    @OneToMany(mappedBy = "designationMaster",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<UserPrivilege> userPrivilege;

    @JsonIgnore
    @OneToMany(mappedBy = "designationMaster",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<EmployeeMaster> employeeMaster;
    
}
