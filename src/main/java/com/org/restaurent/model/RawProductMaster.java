package com.org.restaurent.model;

import io.swagger.models.auth.In;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "raw_product_master")
public class RawProductMaster {

    @Id
    @Column(name = "r_product_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer productId;

    @Column(name = "r_product_name")
        private String productName;

    @Column(name = "r_product_cost")
    private String productCost;

    @Column(name = "r_product_quantity")
    private String productQuantity;

    @Column(name = "r_product_total_cost")
    private String productTotalCost;

    @Column(name = "r_product_expiryDate")
    private Date productExpiryDate;

    @Column(name = "r_product_description")
    private String productDescription;

    @Column(name = "r_product_status")
    private String productStatus;

    @JoinColumn(name = "branch_id",referencedColumnName = "branch_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private BranchMaster branchMaster;
}
