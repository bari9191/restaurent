package com.org.restaurent.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "super_admin_privilege")
public class SuperAdminPrivilege {

    @Id
    @Column(name = "super_admin_privi_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer superAdminPriviId;

    @Column(name = "s_admin_privi_name")
    private String sAdminPriviName;

    @Column(name = "s_admin_privi_status")
    private String sAdminPriviStatus;

    @JsonIgnore
    @OneToMany(mappedBy = "superAdminPrivilege", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<SuperAdmin> superAdminList;
}
