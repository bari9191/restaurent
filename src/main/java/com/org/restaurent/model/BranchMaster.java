package com.org.restaurent.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "branch_master")
public class BranchMaster {

    @Id
    @Column(name = "branch_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer branchId;

    @Column(name = "branch_name")
    private String branchName;

    @Column(name = "branch_addr")
    private String branchAddr;

    @Column(name = "person_name")
    private String personName;

    @Column(name = "person_contact_no")
    private String personContactNo;

    @Column(name = "person_email_id")
    private String personEmailId;

    @Column(name = "branch_status")
    private String branchStatus;

    @JoinColumn(name = "restaurent_id",referencedColumnName = "restaurent_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private RestaurentMaster restaurentMaster;

    @JsonIgnore
    @OneToMany(mappedBy = "branchMaster",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<ShiftMaster> shiftMaster;

    @JsonIgnore
    @OneToMany(mappedBy = "branchMaster",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<FixedProductMaster> fixedProductMaster;

    @JsonIgnore
    @OneToMany(mappedBy = "branchMaster",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private  List<ExpenceMaster> expenceMaster;


    @JsonIgnore
    @OneToMany(mappedBy = "branchMaster",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private  List<RawProductMaster> rawProductMaster;


    @JsonIgnore
    @OneToMany(mappedBy = "branchMaster",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private  List<EmployeeMaster> employeeMaster;


    @JsonIgnore
    @OneToMany(mappedBy = "branchMaster",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private  List<RtableMaster> rTableMasters;

}
