package com.org.restaurent.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "pre_paid_salary_master")
public class PrePaidSalaryMaster {

    @Id
    @Column(name = "pre_paid_salary_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer prePaidSalaryId;

    @Column(name = "pre_paid_salary_cost")
    private String prePaidSalaryCost;

    @Column(name = "pre_paid_salary_date")
    private String prePaidSalaryDate;

    @JoinColumn(name = "salary_id",referencedColumnName = "salary_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private SalaryMaster salaryMaster;
}
