package com.org.restaurent.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "r_table_wise_bill")
public class RtableWiseBill {

    @Id
    @Column(name = "r_table_wise_bill_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer rtableWiseBillId;

    @Column(name = "r_table_wise_bill_pay_type")
    private String rtableWiseBillPayType;

    @Column(name = "r_table_wise_bill_entry_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date rtableWiseBillEntryDate;

    @Column(name = "r_product_qty")
    private Double productQty = 0.0;

    @Column(name = "r_table_wise_bill_status")
    private String rtableWiseBillStatus;

    @JoinColumn(name = "r_table_id",referencedColumnName = "r_table_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private RtableMaster rtableMaster;

    @JoinColumn(name = "f_product_id",referencedColumnName = "f_product_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private FixedProductMaster fixedProductMaster;
}
