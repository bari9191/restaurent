package com.org.restaurent.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "employee_master")
public class EmployeeMaster {

    @Id
    @Column(name = "employee_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer employeeId;

    @Column(name = "employee_name")
    private String employeeName;

    @Column(name = "employee_address")
    private String employeeAddress;

    @Column(name = "employee_contactNo")
    private String employeeContactNo;

    @Column(name = "employee_salary")
    private String employeeSalary;

    @Column(name = "employee_email")
    private String employeeEmail;

    @JoinColumn(name = "branch_id",referencedColumnName = "branch_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private BranchMaster branchMaster;

    @JoinColumn(name = "designation_id",referencedColumnName = "designation_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private DesignationMaster designationMaster;

    @JoinColumn(name = "shift_id",referencedColumnName = "shift_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private ShiftMaster shiftMaster;

    @JoinColumn(name = "emp_credentials_id",referencedColumnName = "emp_credentials_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private EmpCredentials empCredentials;

    @JsonIgnore
    @OneToMany(mappedBy = "employeeMaster",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<SalaryMaster> salaryMasters;

    @JsonIgnore
    @OneToMany(mappedBy = "employeeMaster",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<EmpAttendance> empAttendanceList;

}

