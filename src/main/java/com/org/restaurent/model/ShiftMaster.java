package com.org.restaurent.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "shift_master")
public class ShiftMaster {

    @Id
    @Column(name = "shift_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer shiftId;

    @Column(name = "shift_time_from")
    private String shiftTimeFrom;

    @Column(name = "shift_time_to")
    private String shiftTimeTo;

    @Column(name = "shift_type")
    private String shiftType;

    @Column(name = "shift_status")
    private String shiftStatus;

    @JoinColumn(name = "branch_id",referencedColumnName = "branch_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private BranchMaster branchMaster;


    @JsonIgnore
    @OneToMany(mappedBy = "shiftMaster",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<EmployeeMaster> employeeMaster;
}
