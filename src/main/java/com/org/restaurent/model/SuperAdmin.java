package com.org.restaurent.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "super_admin")
public class SuperAdmin {

    @Id
    @Column(name = "super_admin_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer superAdminId;

    @Column(name = "s_admin_name")
    private String sAdminName;

    @Column(name = "s_admin_contact_no")
    private String sAdminContactNo;

    @Column(name = "s_admin_user_name")
    private String sAdminUserName;

    @Column(name = "s_admin_password")
    private String sAdminPassword;

    @Column(name = "s_admin_status")
    private String sAdminStatus;

    @JoinColumn(name = "super_admin_privi_id",referencedColumnName = "super_admin_privi_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private SuperAdminPrivilege superAdminPrivilege;

    @JsonIgnore
    @OneToMany(mappedBy = "superAdmin", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RestaurentMaster> restaurentMasters;
}
