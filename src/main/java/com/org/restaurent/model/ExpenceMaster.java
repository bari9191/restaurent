package com.org.restaurent.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "expence_master")
public class ExpenceMaster {

    @Id
    @Column(name = "expence_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer expenceId;

    @Column(name = "expence_name")
    private String expenceName;

    @Column(name = "expence_cost")
    private String expenceCost;

    @Column(name = "expence_description")
    private String expenceDescription;

    @Column(name = "expence_date")
    private Date expenceDate;

    @JoinColumn(name = "branch_id",referencedColumnName = "branch_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private BranchMaster branchMaster;
}
