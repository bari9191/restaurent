package com.org.restaurent.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "restaurent_master")
public class RestaurentMaster {

    @Id
    @Column(name = "restaurent_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer restaurentId;

    @Column(name = "restaurent_name")
    private String restaurentName;

    @Column(name = "restaurent_address")
    private String restaurentAddress;

    @Column(name = "restaurent_email")
    private String restaurentEmail;

    @Column(name = "restaurent_contact_no1")
    private String restaurentContactNo1;

    @Column(name = "restaurent_owner1")
    private String restaurentOwner1;

    @Column(name = "restaurent_contact_no2")
    private String restaurentContactNo2;

    @Column(name = "restaurent_owner2")
    private String restaurentOwner2;

    @JoinColumn(name = "super_admin_id",referencedColumnName = "super_admin_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private SuperAdmin superAdmin;

    @JsonIgnore
    @OneToMany(mappedBy = "restaurentMaster", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<BranchMaster> branchMaster;

    @JsonIgnore
    @OneToMany(mappedBy = "restaurentMaster", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<DesignationMaster> designationMasters;
}
