package com.org.restaurent.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "emp_attendance")
public class EmpAttendance {

    @Id
    @Column(name = "emp_atten_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer empAttenId;

    @Column(name = "atten_date")
    private Date attenDate;

    @Column(name = "atten_status")
    private String attenStatus;

    @JoinColumn(name = "employee_id",referencedColumnName = "employee_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private EmployeeMaster employeeMaster;
}
