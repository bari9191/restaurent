package com.org.restaurent.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "r_table_master")
public class RtableMaster {

    @Id
    @Column(name = "r_table_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer rtableId;

    @Column(name = "r_table_name")
    private String rtableName;

    @Column(name = "r_table_status")
    private String rtableStatus;

    @Column(name = "r_table_availability")
    private String rtableAvailability;

    @JoinColumn(name = "branch_id",referencedColumnName = "branch_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private BranchMaster branchMaster;

    @JsonIgnore
    @OneToMany(mappedBy = "rtableMaster",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<RtableWiseBill> rtableWiseBill;
}
