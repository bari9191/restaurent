package com.org.restaurent.dto.dtoconfiguration;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter @Getter @NoArgsConstructor @AllArgsConstructor
public class ResponseVO<T> {

	private String statusCode;

	private String message;

	private T result;


}
