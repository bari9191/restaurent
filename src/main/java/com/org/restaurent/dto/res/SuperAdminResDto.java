package com.org.restaurent.dto.res;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuperAdminResDto {

    private Integer superAdminId;

    private String sAdminName;

    private String sAdminContactNo;

    private String sAdminStatus;

}
