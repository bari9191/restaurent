package com.org.restaurent.dto.res;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuperAdminPrivilegeResDto {

    private Integer superAdminPriviId;

    private String sAdminPriviStatus;
}
