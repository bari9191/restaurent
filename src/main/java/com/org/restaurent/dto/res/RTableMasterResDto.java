package com.org.restaurent.dto.res;

import com.org.restaurent.model.BranchMaster;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class RTableMasterResDto {


    private Integer rTableId;

    private String rTableName;

    private String rTableStatus;

    private String rTableAvailability;

    private BranchMaster branchMaster;
}
