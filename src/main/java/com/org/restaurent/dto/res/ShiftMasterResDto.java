package com.org.restaurent.dto.res;


import com.org.restaurent.model.BranchMaster;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShiftMasterResDto {

    private Integer shiftId;

    private String shiftTimeFrom;

    private String shiftTimeTo;

    private String shiftType;

    private String shiftStatus;

    private BranchMaster branchMaster;

}
