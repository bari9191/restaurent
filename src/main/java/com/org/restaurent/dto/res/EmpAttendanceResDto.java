package com.org.restaurent.dto.res;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
public class EmpAttendanceResDto {

    private String attenStatus;

    private Integer empAttenId;

    private Integer employeeId;

    private String employeeName;

    private String employeeContactNo;

    private Integer designationId;

    private String designationName;

    private String shiftType;

    private Integer shiftId;

    private Boolean attenFlag=false;
}
