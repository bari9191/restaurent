package com.org.restaurent.dto.res;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class TableDashboardResDto {

    private Integer rTableId;

    private String rTableName;

    private String rTableStatus;

    List<FixedProductMasterResDto> fixedProductMasterResDtoList;


}
