package com.org.restaurent.dto.res;

import com.org.restaurent.model.UserPrivilege;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;


@Getter
@Setter
public class UserLoginResDto {

    private String loginMsg;

    private String userName;

    private String userPass;

    private Integer empCredentialsId;

    private Integer employeeId;

    private String employeeName;

    private String employeeContactNo;

    private String employeeEmail;

    private Integer shiftId;

    private String shiftTimeFrom;

    private String shiftTimeTo;

    private String shiftType;

    private Integer branchId;

    private String branchName;

    private Integer designationId;

    private String designationName;

    private String userLoginStatus;

    private UserPrivilegeResDto userPrivilegeResDto;

    private Boolean loginStatus;

    private Integer restaurentId;

    private String restaurentName;
}
