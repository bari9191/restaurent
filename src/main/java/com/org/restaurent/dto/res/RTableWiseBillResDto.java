package com.org.restaurent.dto.res;


import com.org.restaurent.model.FixedProductMaster;
import com.org.restaurent.model.RtableMaster;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RTableWiseBillResDto {


    private Integer rTableWiseBillId;

    private String rTableWiseBillPayType;

    private Date rTableWiseBillEntryDate;

    private String rTableWiseBillStatus;

    private RtableMaster rTableMaster;

    private FixedProductMaster fixedProductMaster;
}
