package com.org.restaurent.dto.res;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryMasterResDto {

    private Integer salaryId;

    private String finalSalary;
}
