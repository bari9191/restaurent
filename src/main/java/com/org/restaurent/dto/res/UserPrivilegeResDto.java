package com.org.restaurent.dto.res;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPrivilegeResDto {

    private Integer userPriviId;

    private Boolean uMaster;

    private String uPriviStatus;
}
