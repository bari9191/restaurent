package com.org.restaurent.dto.res;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PrePaidSalaryMasterResDto {

    private Integer prePaidSalaryId;

    private String prePaidSalaryCost;

    private String prePaidSalaryDate;
}
