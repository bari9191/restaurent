package com.org.restaurent.dto.res;


import com.org.restaurent.model.RestaurentMaster;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class DesignationMasterResDto {

    private Integer designationId;

    private String designationName;

    private String designationStatus;

    private RestaurentMaster restaurentMaster;


}
