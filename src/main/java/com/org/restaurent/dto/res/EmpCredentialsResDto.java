package com.org.restaurent.dto.res;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmpCredentialsResDto {

    private Integer empCredentialsId;

    private String userName;

    private String userPass;

    private String userLoginStatus;
}
