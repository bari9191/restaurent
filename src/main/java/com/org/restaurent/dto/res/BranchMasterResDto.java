package com.org.restaurent.dto.res;

import com.org.restaurent.model.RestaurentMaster;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class BranchMasterResDto {


    private Integer branchId;

    private String branchName;

    private String branchAddr;

    private String personName;

    private String personContactNo;

    private String personEmailId;

    private String branchStatus;

    private RestaurentMaster restaurentMaster;

}
