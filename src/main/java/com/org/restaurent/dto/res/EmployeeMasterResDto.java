package com.org.restaurent.dto.res;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeMasterResDto {

    private Integer employeeId;

    private String employeeName;

    private String employeeAddress;

    private String employeeContactNo;

    private String employeeSalary;

    private String employeeEmail;

}
