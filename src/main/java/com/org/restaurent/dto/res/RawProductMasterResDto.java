package com.org.restaurent.dto.res;

import lombok.Getter;
        import lombok.Setter;

        import java.util.Date;

@Getter
@Setter
public class RawProductMasterResDto {

    private Integer productId;

    private String productName;

    private String productCost;

    private String productQuantity;

    private String productTotalCost;

    private Date productExpiryDate;

    private String productDescription;

    private String productStatus;

}
