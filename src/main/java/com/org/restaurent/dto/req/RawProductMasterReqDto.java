package com.org.restaurent.dto.req;

import com.org.restaurent.model.BranchMaster;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RawProductMasterReqDto {

    private Integer productId;

    private String productName;

    private String productCost;

    private String productQuantity;

    private String productTotalCost;

    private Date productExpiryDate;

    private String productDescription;

    private String productStatus;

    private BranchMaster branchMaster;

}
