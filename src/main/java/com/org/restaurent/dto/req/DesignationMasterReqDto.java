package com.org.restaurent.dto.req;

import com.org.restaurent.model.RestaurentMaster;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DesignationMasterReqDto {

    private Integer designationId;

    private String designationName;

    private String designationStatus;

    private RestaurentMaster restaurentMaster;

}
