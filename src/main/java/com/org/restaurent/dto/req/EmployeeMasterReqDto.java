package com.org.restaurent.dto.req;

import com.org.restaurent.model.BranchMaster;
import com.org.restaurent.model.DesignationMaster;
import com.org.restaurent.model.EmpCredentials;
import com.org.restaurent.model.ShiftMaster;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeMasterReqDto {

    private Integer employeeId;

    private String employeeName;

    private String employeeAddress;

    private String employeeContactNo;

    private String employeeSalary;

    private String employeeEmail;

    private BranchMaster branchMaster;

    private DesignationMaster designationMaster;

    private ShiftMaster shiftMaster;

    private EmpCredentials empCredentials;

}
