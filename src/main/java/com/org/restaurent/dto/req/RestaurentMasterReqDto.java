package com.org.restaurent.dto.req;

import com.org.restaurent.model.SuperAdmin;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestaurentMasterReqDto {

    private Integer restaurentId;

    private String restaurentName;

    private String restaurentAddress;

    private String restaurentEmail;

    private String restaurentContactNo1;

    private String restaurentOwner1;

    private String restaurentContactNo2;

    private String restaurentOwner2;

    private SuperAdmin superAdmin;

}
