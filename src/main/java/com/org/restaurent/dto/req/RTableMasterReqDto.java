package com.org.restaurent.dto.req;

import com.org.restaurent.model.BranchMaster;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RTableMasterReqDto {

    private Integer rtableId;

    private String rtableName;

    private String rtableStatus;

    private String rtableAvailability;

    private BranchMaster branchMaster;

}
