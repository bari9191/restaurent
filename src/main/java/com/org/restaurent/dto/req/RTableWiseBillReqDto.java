package com.org.restaurent.dto.req;

import com.org.restaurent.model.FixedProductMaster;
import com.org.restaurent.model.RtableMaster;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class RTableWiseBillReqDto {

    private Integer rtableId;

    private Integer productId;

    private Double productQty;

    private Integer rTableWiseBillId;

    private String rTableWiseBillPayType;

    private Date rTableWiseBillEntryDate;

    private String rTableWiseBillStatus;

    private RtableMaster rTableMaster;

    private FixedProductMaster fixedProductMaster;

    private String rtableAvailability;

    private List<RTableWiseBillReqDto> rtableWiseBillReqDtoList;

}
