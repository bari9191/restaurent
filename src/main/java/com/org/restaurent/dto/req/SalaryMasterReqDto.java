package com.org.restaurent.dto.req;

import com.org.restaurent.model.EmployeeMaster;
import com.org.restaurent.model.PrePaidSalaryMaster;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryMasterReqDto {

    private Integer salaryId;

    private String finalSalary;

    private EmployeeMaster employeeMaster;

    private PrePaidSalaryMaster prePaidSalaryMaster;

}
