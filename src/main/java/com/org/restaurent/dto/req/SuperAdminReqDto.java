package com.org.restaurent.dto.req;

import com.org.restaurent.model.SuperAdminPrivilege;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuperAdminReqDto {

    private Integer superAdminId;

    private String sAdminName;

    private String sAdminContactNo;

    private String sAdminUserName;

    private String sAdminPassword;

    private String sAdminStatus;

    private SuperAdminPrivilege superAdminPrivilege;
}
