package com.org.restaurent.dto.req;

import com.org.restaurent.model.EmployeeMaster;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PrePaidSalaryMasterReqDto {

    private Integer prePaidSalaryId;

    private String prePaidSalaryCost;

    private String prePaidSalaryDate;

    private EmployeeMaster employeeMaster;

}
