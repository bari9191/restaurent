package com.org.restaurent.dto.req;

import com.org.restaurent.model.DesignationMaster;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPrivilegeReqDto {

    private Integer userPriviId;

    private Boolean uMaster;

    private String uPriviStatus;

    private DesignationMaster designationMaster;

}
