package com.org.restaurent.dto.req;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class EmpAttendanceReqDto {

    private Integer branchId;

    private Date attenDate;

    private String attenStatus;

    private Integer empAttenId;

    private Integer employeeId;

    private String employeeName;

    private String employeeContactNo;

    private Integer designationId;

    private String designationName;

    private String shiftType;

    private Integer shiftId;

    private Boolean attenFlag=false;

    private List<EmpAttendanceReqDto> empAttendanceReqDtoList;
}
