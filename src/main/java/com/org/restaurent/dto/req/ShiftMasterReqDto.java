package com.org.restaurent.dto.req;

import com.org.restaurent.model.BranchMaster;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShiftMasterReqDto {

    private Integer shiftId;

    private String shiftTimeFrom;

    private String shiftTimeTo;

    private String shiftType;

    private String shiftStatus;

    private BranchMaster branchMaster;

}
