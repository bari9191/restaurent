package com.org.restaurent.dto.req;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuperAdminPrivilegeReqDto {

    private Integer superAdminPriviId;

    private String sAdminPriviStatus;

}
