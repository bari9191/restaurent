package com.org.restaurent.dto.req;

import com.org.restaurent.model.BranchMaster;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ExpenceMasterReqDto {

    private Integer expenceId;

    private String expenceName;

    private String expenceCost;

    private String expenceDescription;

    private Date expenceDate;

    private BranchMaster branchMaster;

}
