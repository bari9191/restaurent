package com.org.restaurent.dto.req;

import com.org.restaurent.model.BranchMaster;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FixedProductMasterReqDto {


    private Integer productId;
    private String productName;
    private Double productCost;
    private String productStatus;
    private BranchMaster branchMaster;

}
