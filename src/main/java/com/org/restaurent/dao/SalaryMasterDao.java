package com.org.restaurent.dao;

import java.util.List;

public interface SalaryMasterDao {
    List getSalaryMasterListDao();

    List getSalaryMasterByIdListDao(Integer salaryId);
}
