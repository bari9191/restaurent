package com.org.restaurent.dao;

import java.util.List;

public interface ExpenceMasterDao {
    List getExpenceMasterListDao();

    List getExpenceMasterByIdListDao(Integer expenceId);

    List getTotalExpencetillDateDao(Integer branchId);

    List getExpencebranchWiseDao(Integer branchId);
}
