package com.org.restaurent.dao;

import com.org.restaurent.dto.dtoconfiguration.CommonClass;

public interface CommonDao {

    Boolean createCommonDao(CommonClass commonClass);

    Boolean updateCommonDao(CommonClass commonClass);

    Boolean saveAndUpdate(CommonClass commonClass);

    Boolean deleteCommonDao(CommonClass commonClass);



}
