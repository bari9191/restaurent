package com.org.restaurent.dao;

import com.org.restaurent.dto.res.RestaurentMasterResDto;

import java.util.List;

public interface RestaurentMasterDao {


    List getRestaurentMasterListDao();

    RestaurentMasterResDto getRestaurentMasterByIdListDao(Integer restaurentId);
}
