package com.org.restaurent.dao;

import com.org.restaurent.dto.req.EmpAttendanceReqDto;
import com.org.restaurent.dto.res.EmpAttendanceResDto;

import java.util.List;

public interface EmployeeMasterDao {
    List getEmployeeMasterListDao(Integer branchId);

    List getEmployeeMasterByIdListDao(Integer employeeId);

    List getActiveEmployeeMasterListDao(Integer branchId);

    List<EmpAttendanceResDto> getempAttenListByDateDao(EmpAttendanceReqDto empAttendanceReqDto);

    List<EmpAttendanceResDto> getempDataDao(EmpAttendanceReqDto empAttendanceReqDto);

//    List getEmployeeMasterByIdListDao();
}
