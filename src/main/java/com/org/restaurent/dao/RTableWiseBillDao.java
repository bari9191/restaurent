package com.org.restaurent.dao;

import java.util.List;

public interface RTableWiseBillDao {
    List getRTableWiseBillListDao();

    List getRTableWiseBillByIdListDao(Integer rTableWiseBillId);
}
