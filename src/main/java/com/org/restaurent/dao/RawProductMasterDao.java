package com.org.restaurent.dao;

import java.util.List;

public interface RawProductMasterDao {
    List getRawProductMasterListDao();

    List getRawProductMasterByIdListDao(Integer rProductId);

    List getRawProductMasterbranchwiseListDao(Integer branchId);
}
