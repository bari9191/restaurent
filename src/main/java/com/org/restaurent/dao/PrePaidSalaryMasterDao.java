package com.org.restaurent.dao;

import java.util.List;

public interface PrePaidSalaryMasterDao {
    List getPrePaidSalaryMasterListDao();

    List getPrePaidSalaryMasterByIdListDao(Integer prePaidSalaryId);
}
