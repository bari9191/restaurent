package com.org.restaurent.dao;

import java.util.List;

public interface SuperAdminPrivilegeDao {
    List getSuperAdminPrivilegeListDao();

    List getSuperAdminPrivilegeByIdListDao(Integer superAdminPriviId);
}
