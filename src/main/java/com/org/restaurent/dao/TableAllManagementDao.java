package com.org.restaurent.dao;

import com.org.restaurent.dto.req.RTableMasterReqDto;
import com.org.restaurent.dto.req.RTableWiseBillReqDto;
import com.org.restaurent.dto.res.TableDashboardResDto;

import java.util.List;

public interface TableAllManagementDao {

    List getTableWiseItemsDao(Integer rtableId);

    Boolean updateTableWiseOrderItemsDao(Integer rtableWiseBillId);

    Boolean updateTableStatusDao(RTableWiseBillReqDto rTableWiseBillReqDto1);

    Boolean updateRTableWiseBillStatusDao(RTableMasterReqDto rTableMasterReqDto);
}
