package com.org.restaurent.dao;

import java.util.List;

public interface FixedProductMasterDao {
    List getFixedProductMasterListDao(int branchId);

    List getFixedProductMasterByIdListDao(Integer productId);

    List getfixedProductMasterActiveListDao(int branchId);
}
