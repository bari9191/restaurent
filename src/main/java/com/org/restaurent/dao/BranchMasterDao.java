package com.org.restaurent.dao;

import com.org.restaurent.dto.res.BranchMasterResDto;

import java.util.List;

public interface BranchMasterDao {

    List getBranchMasterListDao();

    BranchMasterResDto getBranchMasterByIdDao(Integer branchId);
}
