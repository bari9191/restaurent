package com.org.restaurent.dao;

import java.util.List;

public interface SuperAdminDao {
    List getSuperAdminListDao();

    List getSuperAdminByIdListDao(Integer superAdminId);
}
