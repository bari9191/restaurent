package com.org.restaurent.dao;


import com.org.restaurent.dto.req.EmpCredentialsReqDto;
import com.org.restaurent.dto.req.UserLoginReqDto;
import com.org.restaurent.dto.res.EmpCredentialsResDto;
import com.org.restaurent.dto.res.UserLoginResDto;
import com.org.restaurent.dto.res.UserPrivilegeResDto;

public interface LoginDao {

    UserLoginResDto getUserNameLoginDao(UserLoginReqDto userLoginReqDto);

    UserPrivilegeResDto getLoginPrivilageDao(UserLoginResDto userLoginResDto);

    EmpCredentialsResDto getuserNameBymobileno(String employeeContactNo);

    boolean updatePasswordDao(EmpCredentialsReqDto empCredentialsReqDto);
}
