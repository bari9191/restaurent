package com.org.restaurent.dao;

import java.util.List;

public interface RTableMasterDao {
    List getRTableMasterListDao(Integer branchId);

    List getRTableMasterByIdListDao(Integer rTableId);

    List rTableMasterActiveListDao(Integer branchId);
}
