package com.org.restaurent.dao;

import java.util.List;

public interface EmpCredentialsDao {
    List getEmpCredentialsListDao();

    List getEmpCredentialsByIdListDao(Integer empCredentialsId);
}
