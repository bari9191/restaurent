package com.org.restaurent.dao;

import java.util.List;

public interface ShiftMasterDao {
    List getShiftMasterListDao();

    List getShiftMasterByIdListDao(Integer shiftId);
}
