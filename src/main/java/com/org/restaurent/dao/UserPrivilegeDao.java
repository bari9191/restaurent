package com.org.restaurent.dao;

import java.util.List;

public interface UserPrivilegeDao {
    List getUserPrivilegeListDao();

    List getUserPrivilegeByIdListDao(Integer userPriviId);
}
