package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.RawProductMasterDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RawProductMasterDaoImpl implements RawProductMasterDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getRawProductMasterListDao() {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select rpm.productId as productId,rpm.productName as productName,rpm.productCost as productCost,rpm.productQuantity as productQuantity,rpm.productTotalCost as productTotalCost,rpm.productExpiryDate as productExpiryDate,rpm.productDescription as productDescription,rpm.productStatus as productStatus from RawProductMaster as rpm");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
            return list;
    }

    @Override
    public List getRawProductMasterByIdListDao(Integer productId) {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select rpm.productId as productId,rpm.productName as productName,rpm.productCost as productCost,rpm.productQuantity as productQuantity,rpm.productTotalCost as productTotalCost,rpm.productExpiryDate as productExpiryDate,rpm.productDescription as productDescription,rpm.productStatus as productStatus from RawProductMaster as rpm where rpm.productId=:productId");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            query.setParameter("productId",productId);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }

    @Override
    public List getRawProductMasterbranchwiseListDao(Integer branchId) {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select rpm.productId as productId,rpm.productName as productName,rpm.productCost as productCost,rpm.productQuantity as productQuantity,rpm.productTotalCost as productTotalCost,rpm.productExpiryDate as productExpiryDate,rpm.productDescription as productDescription,rpm.productStatus as productStatus from RawProductMaster as rpm  inner join rpm.branchMaster as bm where bm.branchId =: branchId ");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            query.setParameter("branchId",branchId);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }
}
