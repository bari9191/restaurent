package com.org.restaurent.dao.impl;


import com.org.restaurent.dao.CommonDao;
import com.org.restaurent.dto.dtoconfiguration.CommonClass;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository @Transactional
public class CommonDaoImpl implements CommonDao {

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public Boolean createCommonDao(CommonClass commonClass) {
        Session session = null;

        try {
            session = sessionFactory.openSession();
            Transaction t = session.beginTransaction();
            session.save(commonClass.getCommonClass());
            t.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            session.close();
        }
    }

    @Override
    public Boolean updateCommonDao(CommonClass commonClass) {
        Session session = null;

        try {
            session = sessionFactory.openSession();
            Transaction t = session.beginTransaction();
            session.update(commonClass.getCommonClass());
            t.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            session.close();
        }
    }

    @Override
    public Boolean saveAndUpdate(CommonClass commonClass) {
        Session session = null;

        try {
            session = sessionFactory.openSession();
            Transaction t = session.beginTransaction();
            session.saveOrUpdate(commonClass.getCommonClass());
            t.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            session.close();
        }
    }

    @Override
    public Boolean deleteCommonDao(CommonClass commonClass) {


        Session session = null;

        try {
            session = sessionFactory.openSession();
            Transaction t = session.beginTransaction();
            session.delete(commonClass.getCommonClass());
            System.out.println("Delete  Successful"+commonClass.getCommonClass());

            t.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            session.close();
        }
    }




}
