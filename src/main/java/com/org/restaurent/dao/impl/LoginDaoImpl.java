package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.LoginDao;
import com.org.restaurent.dto.req.EmpCredentialsReqDto;
import com.org.restaurent.dto.req.UserLoginReqDto;
import com.org.restaurent.dto.res.EmpCredentialsResDto;
import com.org.restaurent.dto.res.UserLoginResDto;
import com.org.restaurent.dto.res.UserPrivilegeResDto;
import com.org.restaurent.model.EmpCredentials;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LoginDaoImpl implements LoginDao {

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public UserLoginResDto getUserNameLoginDao(UserLoginReqDto userLoginReqDto) {
        System.out.println("Inside Dao");

        Session session = null;

        try {
            session = sessionFactory.openSession();

            org.hibernate.Query query = session.createQuery("select em.employeeId as employeeId,em.employeeName as employeeName,em.employeeContactNo as employeeContactNo,em.employeeEmail as employeeEmail,dm.designationId as designationId,dm.designationName as designationName,bm.branchId as branchId,bm.branchName as branchName,sm.shiftId as shiftId,sm.shiftTimeFrom as shiftTimeFrom,sm.shiftTimeTo as shiftTimeTo,sm.shiftType as shiftType,ec.empCredentialsId as empCredentialsId,ec.userName as userName,ec.userPass as userPass,ec.userLoginStatus as userLoginStatus,rm.restaurentId as restaurentId,rm.restaurentName as restaurentName from EmployeeMaster as em inner join em.branchMaster as bm inner join bm.restaurentMaster as rm inner join em.designationMaster as dm inner join em.shiftMaster as sm inner join em.empCredentials as ec where ec.userName=:userName and ec.userPass=:userPass");
            query.setParameter("userName", userLoginReqDto.getUserName());
            query.setParameter("userPass",userLoginReqDto.getUserPass());
            query.setResultTransformer(Transformers.aliasToBean(UserLoginResDto.class));

            UserLoginResDto userLoginResDto = (UserLoginResDto) query.uniqueResult();

            return userLoginResDto;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    @Override
    public UserPrivilegeResDto getLoginPrivilageDao(UserLoginResDto userLoginResDto) {
        Session session = null;
        UserPrivilegeResDto userPrivilegeResDto = new UserPrivilegeResDto();

        try {
            session = sessionFactory.openSession();

            Query query = session.createQuery("select up.userPriviId as userPriviId,up.uMaster as uMaster,up.uPriviStatus as uPriviStatus from UserPrivilege as up where up.designationMaster.designationId=:designationId ");

            query.setParameter("designationId", userLoginResDto.getDesignationId());
            query.setResultTransformer(Transformers.aliasToBean(UserPrivilegeResDto.class));

            userPrivilegeResDto = (UserPrivilegeResDto) query.uniqueResult();

            return userPrivilegeResDto;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    @Override
    public EmpCredentialsResDto getuserNameBymobileno(String employeeContactNo) {
        Session session=null;
        EmpCredentialsResDto empCredentialsResDto = new EmpCredentialsResDto();
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select  ec.empCredentialsId as empCredentialsId,ec.userName as userName,ec.userPass as userPass,ec.userLoginStatus as userLoginStatus  from EmployeeMaster as em inner join em.empCredentials as ec where em.employeeContactNo =:employeeContactNo");
            query.setParameter("employeeContactNo",employeeContactNo);
            query.setResultTransformer(Transformers.aliasToBean(EmpCredentialsResDto.class));
            empCredentialsResDto = (EmpCredentialsResDto) query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return empCredentialsResDto;
    }

    @Override
    public boolean updatePasswordDao(EmpCredentialsReqDto empCredentialsReqDto) {
        Session session = null;
            try {
                session = sessionFactory.openSession();
                Transaction transaction = session.beginTransaction();

                Query query = session.createQuery("Update EmpCredentials as ec set ec.userPass=:userPass where ec.empCredentialsId=:empCredentialsId ");
                query.setParameter("userPass",empCredentialsReqDto.getUserPass());
                query.setParameter("empCredentialsId",empCredentialsReqDto.getEmpCredentialsId());
                System.out.println(query.executeUpdate());
                transaction.commit();

                return true;

            }
            catch (Exception e){
            e.printStackTrace();
            }
            finally {
                    session.close();
            }

        return false;
    }



}


