package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.ExpenceMasterDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ExpenceMasterDaoImpl implements ExpenceMasterDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getExpenceMasterListDao() {
        Session session=null;
        List list=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select em.expenceId as expenceId,em.expenceName as expenceName,em.expenceCost as expenceCost,em.expenceDescription as expenceDescription from ExpenceMaster as em");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

            list = query.list();


        } catch (Exception e) {
            e.printStackTrace();

        }
        finally {
            session.close();
        }

        return list;
        }

    @Override
    public List getExpenceMasterByIdListDao(Integer expenceId) {
        Session session=null;
        List list=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select em.expenceId as expenceId,em.expenceName as expenceName,em.expenceCost as expenceCost,em.expenceDescription as expenceDescription ,em.expenceDate as expenceDate from ExpenceMaster as em where em.expenceId=:expenceId");
            query.setParameter("expenceId",expenceId);
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

            list = query.list();


        } catch (Exception e) {
            e.printStackTrace();

        }
        finally {
            session.close();
        }

        return list;
    }

    @Override
    public List getTotalExpencetillDateDao(Integer branchId) {
        Session session=null;
        List list=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select sum(em.expenceCost) as expenceCost from ExpenceMaster as em inner join em.branchMaster as bm where bm.branchId =:branchId");
            query.setParameter("branchId",branchId);
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

            list = query.list();


        } catch (Exception e) {
            e.printStackTrace();

        }
        finally {
            session.close();
        }

        return list;
    }

    @Override
    public List getExpencebranchWiseDao(Integer branchId) {
        Session session=null;
        List list=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select em.expenceId as expenceId,em.expenceName as expenceName,em.expenceCost as expenceCost,em.expenceDescription as expenceDescription ,em.expenceDate as expenceDate from ExpenceMaster as em inner join em.branchMaster as bm where bm.branchId =:branchId");
            query.setParameter("branchId",branchId);
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

            list = query.list();


        } catch (Exception e) {
            e.printStackTrace();

        }
        finally {
            session.close();
        }

        return list;
    }

}
