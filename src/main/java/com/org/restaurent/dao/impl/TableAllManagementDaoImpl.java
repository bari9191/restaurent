package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.TableAllManagementDao;
import com.org.restaurent.dto.req.RTableMasterReqDto;
import com.org.restaurent.dto.req.RTableWiseBillReqDto;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TableAllManagementDaoImpl implements TableAllManagementDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getTableWiseItemsDao(Integer rtableId) {
        Session session=null;
        List list=null;
        try {
            session = sessionFactory.openSession();

            org.hibernate.Query query = session.createQuery("select rtwb.rtableWiseBillId as rtableWiseBillId,rtwb.rtableWiseBillEntryDate as rtableWiseBillEntryDate,rtwb.rtableWiseBillStatus as rtableWiseBillStatus,rtwb.rtableWiseBillPayType as rtableWiseBillPayType,rtm.rtableId as rtableId,rtm.rtableName as rtableName,fpm.productId as productId,fpm.productName as productName,fpm.productCost as productCost,fpm.productStatus as productStatus,rtwb.productQty as productQty from RtableWiseBill as rtwb inner join rtwb.rtableMaster as rtm inner join rtwb.fixedProductMaster as fpm where rtm.rtableId=:rtableId and rtwb.rtableWiseBillStatus='सक्रिय' ");
            query.setParameter("rtableId",rtableId);

            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

            list = query.list();


        } catch (Exception e) {
            e.printStackTrace();

        }
        finally {
            session.close();
        }

        return list;
    }

    @Override
    public Boolean updateTableWiseOrderItemsDao(Integer rtableWiseBillId) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            String hql = "update RtableWiseBill as rtwb set rtwb.rtableWiseBillStatus='हटविले' where rtwb.rtableWiseBillId=:rtableWiseBillId ";
            org.hibernate.Query query = session.createQuery(hql);
            query.setParameter("rtableWiseBillId",rtableWiseBillId);
            System.out.println(query.executeUpdate());
            transaction.commit();
            return true;
        } catch (Throwable t) {
            //transaction.rollback();
            throw t;
        }
        finally {
            session.close();
        }
    }

    @Override
    public Boolean updateTableStatusDao(RTableWiseBillReqDto rTableWiseBillReqDto1) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            String hql = "update RtableMaster as rtm set rtm.rtableAvailability=:rtableAvailability where rtm.rtableId=:rtableId ";
            org.hibernate.Query query = session.createQuery(hql);
            query.setParameter("rtableAvailability",rTableWiseBillReqDto1.getRtableAvailability());
            query.setParameter("rtableId",rTableWiseBillReqDto1.getRtableId());
            System.out.println(query.executeUpdate());
            transaction.commit();
            return true;
        } catch (Throwable t) {
            //transaction.rollback();
            throw t;
        }
        finally {
            session.close();
        }
    }

    @Override
    public Boolean updateRTableWiseBillStatusDao(RTableMasterReqDto rTableMasterReqDto) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            String hql = "update RtableWiseBill as rtwb set rtwb.rtableWiseBillStatus='निष्क्रिय' where rtwb.rtableMaster.rtableId=:rtableId ";
            org.hibernate.Query query = session.createQuery(hql);
            query.setParameter("rtableId",rTableMasterReqDto.getRtableId());
            System.out.println(query.executeUpdate());
            transaction.commit();
            return true;
        } catch (Throwable t) {
            //transaction.rollback();
            throw t;
        }
        finally {
            session.close();
        }
    }
}
