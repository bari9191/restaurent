package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.RestaurentMasterDao;
import com.org.restaurent.dto.res.RestaurentMasterResDto;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RestaurentMasterDaoImpl implements RestaurentMasterDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getRestaurentMasterListDao() {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select rm.restaurentId as restaurentId,rm.restaurentName as restaurentName,rm.restaurentAddress as restaurentAddress,rm.restaurentEmail as restaurentEmail,rm.restaurentContactNo1 as restaurentContactNo1,rm.restaurentOwner1 as restaurentOwner1,rm. restaurentContactNo2 as restaurentContactNo2,rm.restaurentOwner2 as restaurentOwner2 from RestaurentMaster as rm");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
        }

    @Override
    public RestaurentMasterResDto getRestaurentMasterByIdListDao(Integer restaurentId) {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select rm.restaurentId as restaurentId,rm.restaurentName as restaurentName,rm.restaurentAddress as restaurentAddress,rm.restaurentEmail as restaurentEmail,rm.restaurentContactNo1 as restaurentContactNo1,rm.restaurentOwner1 as restaurentOwner1,rm.restaurentContactNo2 as restaurentContactNo2,rm.restaurentOwner2 as restaurentOwner2 from RestaurentMaster as rm where rm.restaurentId=:restaurentId");
            query.setResultTransformer(Transformers.aliasToBean(RestaurentMasterResDto.class));
            query.setParameter("restaurentId",restaurentId);

            RestaurentMasterResDto restaurentMasterResDto = (RestaurentMasterResDto) query.uniqueResult();

            return restaurentMasterResDto;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
        finally {
            session.close();
        }

    }
}
