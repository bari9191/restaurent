package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.SalaryMasterDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SalaryMasterDaoImpl implements SalaryMasterDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getSalaryMasterListDao() {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select sm.salaryId as salaryId,sm.finalSalary as finalSalary from SalaryMaster as sm");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }

    @Override
    public List getSalaryMasterByIdListDao(Integer salaryId) {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select sm.salaryId as salaryId,sm.finalSalary as finalSalary from SalaryMaster as sm where sm.salaryId=:salaryId");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            query.setParameter("salaryId",salaryId);

            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }
}
