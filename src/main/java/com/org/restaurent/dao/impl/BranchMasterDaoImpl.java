package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.BranchMasterDao;
import com.org.restaurent.dto.res.BranchMasterResDto;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BranchMasterDaoImpl implements BranchMasterDao {

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public List getBranchMasterListDao() {
        Session session=null;
        List list=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select bm.branchId as branchId ,bm.branchName as branchName ,bm.branchAddr as branchAddr ,bm.personName as personName ,bm.personContactNo as personContactNo ,bm.personEmailId as personEmailId ,bm.branchStatus as branchStatus  from BranchMaster as bm ");

            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

            list = query.list();


        } catch (Exception e) {
            e.printStackTrace();

        }
        finally {
            session.close();
        }

        return list;
    }

    @Override
    public BranchMasterResDto getBranchMasterByIdDao(Integer branchId) {
        Session session=null;
        BranchMasterResDto branchMasterResDto=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select bm.branchId as branchId ,bm.branchName as branchName ,bm.branchAddr as branchAddr ,bm.personName as personName ,bm.personContactNo as personContactNo ,bm.personEmailId as personEmailId ,bm.branchStatus as branchStatus  from BranchMaster as bm where bm.branchId=:branchId");
            query.setParameter("branchId",branchId);
            query.setResultTransformer(Transformers.aliasToBean(BranchMasterResDto.class));

            branchMasterResDto = (BranchMasterResDto) query.uniqueResult();


        } catch (Exception e) {
            e.printStackTrace();

        }
        finally {
            session.close();
        }

        return branchMasterResDto;
    }
}
