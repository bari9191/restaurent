package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.RTableMasterDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RTableMasterDaoImpl implements RTableMasterDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getRTableMasterListDao(Integer branchId) {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select rtm.rtableId as rtableId,rtm.rtableName as rtableName,rtm.rtableStatus as rtableStatus,rtm.rtableAvailability as rtableAvailability from RtableMaster as rtm");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
        }

    @Override
    public List getRTableMasterByIdListDao(Integer rTableId) {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select rtm.rtableId as rtableId,rtm.rtableName as rtableName,rtm.rtableStatus as rtableStatus,rtm.rtableAvailability as rtableAvailability from RtableMaster as rtm where rtm.rTableId=:rTableId");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            query.setParameter("rTableId",rTableId);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }

    @Override
    public List rTableMasterActiveListDao(Integer branchId) {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select rtm.rtableId as rtableId,rtm.rtableName as rtableName,rtm.rtableStatus as rtableStatus,rtm.rtableAvailability as rtableAvailability from RtableMaster as rtm where rtm.rtableStatus='सक्रिय'");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }
}
