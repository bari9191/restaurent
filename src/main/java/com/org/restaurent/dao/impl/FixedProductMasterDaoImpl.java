package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.FixedProductMasterDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FixedProductMasterDaoImpl implements FixedProductMasterDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getFixedProductMasterListDao(int branchId) {
        Session session=null;
        List list=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select fpm.productId as productId,fpm.productName as productName,fpm.productCost as productCost,fpm.productStatus as productStatus from FixedProductMaster as fpm inner join fpm.branchMaster as bm where bm.branchId=:branchId");
            query.setParameter("branchId",branchId);
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

            list = query.list();


        } catch (Exception e) {
            e.printStackTrace();

        }
        finally {
            session.close();
        }

        return list;
    }

    @Override
    public List getFixedProductMasterByIdListDao(Integer fproductId) {
        Session session=null;
        List list=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select fpm.fproductId as fproductId,fpm.fProductName as fProductName,fpm.fProductCost as fProductCost,fpm.fProductStatus as fProductStatus  from FixedProductMaster as fpm where fpm.fproductId=:fproductId");
            query.setParameter("fproductId",fproductId);
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

            list = query.list();


        } catch (Exception e) {
            e.printStackTrace();

        }
        finally {
            session.close();
        }

        return list;
    }

    @Override
    public List getfixedProductMasterActiveListDao(int branchId) {
        Session session=null;
        List list=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select fpm.productId as productId,fpm.productName as productName,fpm.productCost as productCost,fpm.productStatus as productStatus from FixedProductMaster as fpm inner join fpm.branchMaster as bm where bm.branchId=:branchId and fpm.productStatus='सक्रीय'");
            query.setParameter("branchId",branchId);
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

            list = query.list();


        } catch (Exception e) {
            e.printStackTrace();

        }
        finally {
            session.close();
        }

        return list;
    }
}
