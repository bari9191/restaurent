package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.DesignationMasterDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DesignationMasterDaoImpl implements DesignationMasterDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getDesignationMasterListDao() {
        Session session=null;
        List list=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select dm.designationId as designationId,dm.designationName as designationName,dm.designationStatus as designationStatus from DesignationMaster as dm");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

            list = query.list();


        } catch (Exception e) {
            e.printStackTrace();

        }
        finally {
            session.close();
        }

        return list;
    }

    @Override
    public List getDesignationMasterByIdListDao(Integer restaurentId) {
        Session session=null;
        List list=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select dm.designationId as designationId,dm.designationName as designationName,dm.designationStatus as designationStatus from DesignationMaster as dm  inner join dm.restaurentMaster as rm where rm.restaurentId=:restaurentId");
            query.setParameter("restaurentId",restaurentId);
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

            list = query.list();


        } catch (Exception e) {
            e.printStackTrace();

        }
        finally {
            session.close();
        }

        return list;
    }
}
