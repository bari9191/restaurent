package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.RTableWiseBillDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RTableWiseBillDaoImpl implements RTableWiseBillDao {


    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getRTableWiseBillListDao() {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select rtwb.rTableWiseBillId as rTableWiseBillId,rtwb.rTableWiseBillPayType as rTableWiseBillPayType,rtwb.rTableWiseBillStatus as rTableWiseBillStatus from RTableWiseBill as rtwb");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }

    @Override
    public List getRTableWiseBillByIdListDao(Integer rTableWiseBillId) {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select rtwb.rTableWiseBillId as rTableWiseBillId,rtwb.rTableWiseBillPayType as rTableWiseBillPayType,rtwb.rTableWiseBillStatus as rTableWiseBillStatus from RTableWiseBill as rtwb where rtwb.rTableWiseBillId=:rTableWiseBillId");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            query.setParameter("rTableWiseBillId",rTableWiseBillId);

            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }
}
