package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.SuperAdminDao;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SuperAdminDaoImpl implements SuperAdminDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getSuperAdminListDao() {
        Session session = null;
        List list=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select  sa.superAdminId as superAdminId,sa.sAdminName as sAdminName, sa.sAdminContactNo as sAdminContactNo,sa.sAdminUserName as sAdminUserName,sa.sAdminPassword as sAdminPassword,sa.sAdminStatus as sAdminStatus  from SuperAdmin as sa ");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

            list = query.list();

        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }

        return list;
    }

    @Override
    public List getSuperAdminByIdListDao(Integer superAdminId) {
        Session session = null;
        List list=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select  sa.superAdminId as superAdminId,sa.sAdminName as sAdminName, sa.sAdminContactNo as sAdminContactNo,sa.sAdminUserName as sAdminUserName,sa.sAdminPassword as sAdminPassword,sa.sAdminStatus as sAdminStatus  from SuperAdmin as sa where sa.superAdminId=:superAdminId ");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            query.setParameter("superAdminId",superAdminId);
            list = query.list();

        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }

        return list;


    }
}

