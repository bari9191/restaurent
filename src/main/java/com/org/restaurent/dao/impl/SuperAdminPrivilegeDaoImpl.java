package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.SuperAdminPrivilegeDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SuperAdminPrivilegeDaoImpl implements SuperAdminPrivilegeDao {

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public List getSuperAdminPrivilegeListDao() {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select sap.superAdminPriviId as superAdminPriviId,sap.sAdminPriviStatus as sAdminPriviStatus from SuperAdminPrivilege as sap");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }

    @Override
    public List getSuperAdminPrivilegeByIdListDao(Integer superAdminPriviId) {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select sap.superAdminPriviId as superAdminPriviId,sap.sAdminPriviStatus as sAdminPriviStatus from SuperAdminPrivilege as sap where superAdminPriviId=:superAdminPriviId");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            query.setParameter("superAdminPriviId",superAdminPriviId);

            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }
}
