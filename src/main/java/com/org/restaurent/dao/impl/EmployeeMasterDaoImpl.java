package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.EmployeeMasterDao;
import com.org.restaurent.dto.req.EmpAttendanceReqDto;
import com.org.restaurent.dto.res.EmpAttendanceResDto;
import com.org.restaurent.model.DesignationMaster;
import com.org.restaurent.model.EmpAttendance;
import com.org.restaurent.model.ShiftMaster;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmployeeMasterDaoImpl implements EmployeeMasterDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getEmployeeMasterListDao(Integer branchId) {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select  dm as designationList,ec as empCredentials,sm as shiftList, em.employeeId as employeeId,em.employeeName as employeeName,em.employeeAddress as employeeAddress,em.employeeContactNo as employeeContactNo,em.employeeSalary as employeeSalary,em.employeeEmail as employeeEmail from EmpCredentials as ec inner join  ec.employeeMaster as em inner join em.branchMaster as bm inner join em.designationMaster as dm inner join em.shiftMaster as sm where bm.branchId=:branchId");
            query.setParameter("branchId",branchId);
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }

    @Override
    public List getEmployeeMasterByIdListDao(Integer employeeId) {

            Session session = null;
            List list = null;
            try {
                session = sessionFactory.openSession();
                org.hibernate.Query query = session.createQuery("select em.employeeId as employeeId,em.employeeName as employeeName,em.employeeAddress as employeeAddress,em.employeeContactNo as employeeContactNo,em.employeeSalary as employeeSalary,em.employeeEmail as employeeEmail from EmployeeMaster as em where em.employeeId=:employeeId  ");
                query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
                query.setParameter("employeeId",employeeId);
                list = query.list();
            }
            catch (Exception e){
                e.printStackTrace();
            }
            finally {
                {
                    session.close();

                }
            }
        return list;
    }

    @Override
    public List getActiveEmployeeMasterListDao(Integer branchId) {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select  dm as designationList,ec as empCredentials,sm as shiftList, em.employeeId as employeeId,em.employeeName as employeeName,em.employeeAddress as employeeAddress,em.employeeContactNo as employeeContactNo,em.employeeSalary as employeeSalary,em.employeeEmail as employeeEmail from EmpCredentials as ec inner join  ec.employeeMaster as em inner join em.branchMaster as bm inner join em.designationMaster as dm inner join em.shiftMaster as sm where bm.branchId=:branchId and ec.userLoginStatus='सक्रिय' ");
            query.setParameter("branchId",branchId);
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }

    @Override
    public List<EmpAttendanceResDto> getempAttenListByDateDao(EmpAttendanceReqDto empAttendanceReqDto) {

        Session session = null;
        List<EmpAttendanceResDto> list = null;

        try {

            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select ea.attenStatus as attenStatus,ea.empAttenId as empAttenId,em.employeeId as employeeId,em.employeeName as employeeName,em.employeeContactNo as employeeContactNo,dm.designationId as designationId,dm.designationName as designationName,sm.shiftType as shiftType,sm.shiftId as shiftId from EmpAttendance as ea inner join ea.employeeMaster as em inner join em.branchMaster as bm inner join em.designationMaster as dm inner join em.shiftMaster as sm where bm.branchId=:branchId and date(ea.attenDate)=date(:attenDate) ");
            query.setParameter("attenDate",empAttendanceReqDto.getAttenDate());
            query.setParameter("branchId",empAttendanceReqDto.getBranchId());

            query.setResultTransformer(Transformers.aliasToBean(EmpAttendanceResDto.class));
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }

    @Override
    public List<EmpAttendanceResDto> getempDataDao(EmpAttendanceReqDto empAttendanceReqDto) {
        Session session = null;
        List<EmpAttendanceResDto> list = null;

        try {

            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select em.employeeId as employeeId,em.employeeName as employeeName,em.employeeContactNo as employeeContactNo,dm.designationId as designationId,dm.designationName as designationName,sm.shiftType as shiftType,sm.shiftId as shiftId from EmployeeMaster as em inner join em.branchMaster as bm inner join em.designationMaster as dm inner join em.shiftMaster as sm where bm.branchId=:branchId ");
            query.setParameter("branchId",empAttendanceReqDto.getBranchId());

            query.setResultTransformer(Transformers.aliasToBean(EmpAttendanceResDto.class));
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }
}
