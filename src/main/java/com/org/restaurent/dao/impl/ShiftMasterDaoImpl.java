package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.ShiftMasterDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ShiftMasterDaoImpl implements ShiftMasterDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getShiftMasterListDao() {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select sm.shiftId as shiftId,sm.shiftTimeFrom as shiftTimeFrom,sm.shiftTimeTo as shiftTimeTo,sm.shiftType as shiftType,sm.shiftStatus as shiftStatus from ShiftMaster as sm");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;

    }

    @Override
    public List getShiftMasterByIdListDao(Integer branchId) {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select sm.shiftId as shiftId,sm.shiftTimeFrom as shiftTimeFrom,sm.shiftTimeTo as shiftTimeTo,sm.shiftType as shiftType,sm.shiftStatus as shiftStatus from ShiftMaster as sm inner join sm.branchMaster as bm  where bm.branchId =:branchId");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            query.setParameter("branchId",branchId);

            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }
}
