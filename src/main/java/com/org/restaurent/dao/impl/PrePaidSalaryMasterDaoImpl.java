package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.PrePaidSalaryMasterDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PrePaidSalaryMasterDaoImpl implements PrePaidSalaryMasterDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getPrePaidSalaryMasterListDao() {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select ppsm.prePaidSalaryId as prePaidSalaryId,ppsm.prePaidSalaryCost as prePaidSalaryCost,ppsm.prePaidSalaryDate as prePaidSalaryDate  from PrePaidSalaryMaster as ppsm");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();

        }
        finally {
            session.close();
        }
        return list;
    }

    @Override
    public List getPrePaidSalaryMasterByIdListDao(Integer prePaidSalaryId) {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select ppsm.prePaidSalaryId as prePaidSalaryId,ppsm.prePaidSalaryCost as prePaidSalaryCost,ppsm.prePaidSalaryDate as prePaidSalaryDate  from PrePaidSalaryMaster as ppsm  where prePaidSalaryId=:prePaidSalaryId ");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            query.setParameter("prePaidSalaryId",prePaidSalaryId);
            list = query.list();
        }
        catch (Exception e){
            e.printStackTrace();

        }
        finally {
            session.close();
        }
        return list;
    }
}
