package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.EmpCredentialsDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmpCredentialsDaoImpl implements EmpCredentialsDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getEmpCredentialsListDao() {
        Session session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select ec.empCredentialsId as empCredentialsId,ec.userName as userName,ec.userPass as userPass,ec.userLoginStatus as userLoginStatus from EmpCredentials as ec");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            list = query.list();

        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return list;
    }

    @Override
    public List getEmpCredentialsByIdListDao(Integer empCredentialsId) {

        Session  session = null;
        List list = null;

        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select ec.empCredentialsId as empCredentialsId,ec.userName as userName,ec.userPass as userPass,ec.userLoginStatus as userLoginStatus from EmpCredentials as ec where ec.empCredentialsId=:empCredentialsId");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            query.setParameter("empCredentialsId",empCredentialsId);
            list = query.list();

        }catch (Exception e){
            e.printStackTrace();
        }
      finally {
            session.close();
        }
        return list;
    }
}
