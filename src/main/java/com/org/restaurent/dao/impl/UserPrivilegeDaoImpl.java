package com.org.restaurent.dao.impl;

import com.org.restaurent.dao.UserPrivilegeDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserPrivilegeDaoImpl implements UserPrivilegeDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List getUserPrivilegeListDao() {
        Session session = null;
        List list=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select up.userPriviId as userPriviId,up.uMaster as uMaster,up.uPriviStatus as uPriviStatus from UserPrivilege as up ");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

            list = query.list();

        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }

        return list;
    }

    @Override
    public List getUserPrivilegeByIdListDao(Integer userPriviId) {
        Session session = null;
        List list=null;
        try {
            session = sessionFactory.openSession();
            org.hibernate.Query query = session.createQuery("select up.userPriviId as userPriviId,up.uMaster as uMaster,up.uPriviStatus as uPriviStatus from UserPrivilege as up where up.userPriviId=:userPriviId");
            query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            query.setParameter("userPriviId",userPriviId);

            list = query.list();

        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }

        return list;
    }
}
