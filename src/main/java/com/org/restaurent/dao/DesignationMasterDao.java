package com.org.restaurent.dao;

import java.util.List;

public interface DesignationMasterDao {
    List getDesignationMasterListDao();

    List getDesignationMasterByIdListDao(Integer restaurentId);
}
